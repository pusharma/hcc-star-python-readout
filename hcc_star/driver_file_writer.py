from .driver_base import HCCStarDriverBase

# Outputs the commands into a pre-specified file, as opposed to sending them to the HCC*/ABC*

class HCCStarDriverFileWriter(HCCStarDriverBase):
  output_file_name = "commands.txt"
  file_append = True
  comments = ""

  def issue_flx_config(self, command, reg_name):
    self.report("Driver requested flx-config command, ignoring...", verbosity=2)
    cmd = '{} {} {} -d {}'.format(os.path.join(
      self.path_to_ftools, 'flx-config'), command, reg_name, self.felix_device)
    self.run_in_foreground_shell(cmd)


  def issue_fupload(self, command):
    self.report("Driver requested fupload command, ignoring...", verbosity=2)
    self.report("Writing HCC*/ABC* command to {}".format(self.output_file_name), verbosity=2)
    mode = 'a' if self.file_append else 'w'
    with open(self.output_file_name, mode) as f:
      if self.comments != "":
        f.write("-- {} \n".format(self.comments))
      if isinstance(command, str):
        f.write("{}".format(command))
      else:
        for byte in command.cut(8):
          f.write("{:02X}\n".format(byte.uint))


  def issue_fcheck(self, input_file, block_count):
    self.report("Driver requested fcheck command, ignoring...", verbosity=2)
    return

  def issue_fdaq(self, fdaq_acquisition_time):
    self.report("Driver requested fdaq command, ignoring...", verbosity=2)
    return

  def listen_and_send_command(self, command):
    self.send_command(command)