from bitstring import BitArray, BitStream, Bits, ReadError
from enum import IntEnum
import time

from .codec import swap_nibbles

"""
  The parser interprets a given bitsarray as data coming from HCCStar
  (after FELIX header and trailer have been removed)

  # How to use:

  data = BitArray(filename=args.file_name)
  parser = Parser(data, verbose=False)
  parser.parse()

  # now parser contains all complete packets from the file

  parser.packet_list # array or all packets in the same order
                     # as they appear in the bitstream

  parser.packet_map    # packets grouped by type, as defined in TYP IntEnum
  parser.packet_map[0] # first packet (if there are any)

  parser.packet_map[TYP.HCC_reg_read] # array of all HCC register read packets
                                      # in the order of arrival

  parser.packet_map[TYP.HCC_reg_read][0] # first packet of the type
                                         # (if there are any)

"""


# This enum defines packet types received from HCC*
# use this to retrieve packets by type from the parser's packet_map
class TYP(IntEnum):
  PR = 1
  LP = 2
  ABC_reg_read = 4
  ABC_transparent = 7
  HCC_reg_read = 8
  ABC_full_transparent = 11
  ABC_high_priority = 13
  HCC_high_priority = 14


# throw an exception if the cluster size exceeds this number
# otherwise garbage data causes the entire 60 MB of data from fdaq
# to be recognized as a cluster, and this results in poor performance
max_cluster_size = 4096

hcc_hpr_addr = 0x0f
abc_hpr_addr = 0x3f

# Packet parsing exception (throw when invalid packets are found)
class ParseError(ReadError):
  pass


# Get target type given argument string for regular register reads
def get_target_type(target):
  target_lower = target.lower()
  if target_lower == "hcc":
    return TYP.HCC_reg_read
  elif target_lower == "abc":
    return TYP.ABC_reg_read  
  raise ValueError("Unsupported target {}, supported: HCC, ABC".format(target))


# The parser itself
class Parser():
  def __init__(self, data_BitArray=None, verbose=False, swap_nibbles=False,
    max_parse_time=5, ignore_physics_packets=False, faster=False):
    if data_BitArray is not None:
      if swap_nibbles:
        self.data = BitStream(swap_nibbles(data_BitArray))
      else:
        self.data = BitStream(data_BitArray)
      self.length = self.data.length
    self.packet_list = []
    self.packet_map = {
      TYP.PR : [],
      TYP.LP : [],
      TYP.ABC_reg_read : [],
      TYP.ABC_transparent : [],
      TYP.HCC_reg_read : [],
      TYP.ABC_full_transparent : [],
      TYP.ABC_high_priority : [],
      TYP.HCC_high_priority : []}
    self.verbose = verbose
    self.parse_errors = 0
    self.packets_total = 0
    self.timeout = False
    self.max_parse_time = max_parse_time
    self.ignore_physics_packets = ignore_physics_packets
    self.faster = faster


  # parse the file into an array and map of packets
  def parse(self):
    self.parse_timeout = False
    time_start = time.time()
    while True:
      try:
        self.read_next()
        self.packets_total += 1
        if (self.max_parse_time > 0) and (time.time() - time_start > self.max_parse_time):
          self.parse_timeout = True
          break
      except ParseError as e:
        self.parse_errors += 1
        self.report(e)
      except ReadError as e:      
        self.report(e)
        break        

    self.report("The data contains {} valid HCC*/ABC* packet(s)".format(self.packets_total))
    if self.parse_errors != 0:
      self.report("There were {} parse errors. This number is supposed to be 0. Please check if the data is corrupt.".format(self.parse_errors))
    else:
      self.report("There were no parse errors")


  # find a register read packet for a specific address
  def find_register_read(self, register_id, target=None):
    required_packet_type = get_target_type(target)
    known_packet_types = {
      TYP.ABC_reg_read : self.parse_ABC_register_read,
      TYP.HCC_reg_read : self.parse_HCC_register_read}

    self.parse_timeout = False
    time_start = time.time()
    while True:
      try:
        if (self.max_parse_time > 0) and (time.time() - time_start > self.max_parse_time):
          self.parse_timeout = True
          break
        self.pos = self.data.pos
        self.typ = self.data.read(4)
        old_pos = self.data.bitpos
        if self.typ.uint == required_packet_type:
          packet = known_packet_types[self.typ.uint]()
          if packet['reg_addr'].uint == register_id: 
            return packet
          else:
            self.data.bitpos = old_pos
      except ParseError as e:
        pass
      except ReadError as e:  
        return None

  # ------------ private methods below ---------------------


  # Attempt to interpret the next packet from the stream
  # and pass it to the appropriate function
  def read_next(self):
    self.pos = self.data.pos
    self.typ = self.data.read(4)

    known_packet_types = {
      TYP.PR : self.parse_PR_packet,
      TYP.LP : self.parse_LP_packet,
      TYP.ABC_reg_read : self.parse_ABC_register_read,
      TYP.ABC_transparent : self.parse_ABC_packet_transparent,
      TYP.HCC_reg_read : self.parse_HCC_register_read,
      TYP.ABC_full_transparent : self.parse_ABC_full_transparent,
      TYP.ABC_high_priority : self.parse_ABC_high_priority,
      TYP.HCC_high_priority : self.parse_HCC_high_priority }

    typ = self.typ.uint
    if not typ in known_packet_types:
      raise ParseError("Unknown packet @ 0x{:x}({})".format(self.pos//8, (self.pos % 8) // 4))

    packet = known_packet_types[typ]()
    packet["typ"] = TYP(typ)
    packet["file_pos_bit"] = self.pos
    if not self.faster:
      self.packet_list.append(packet)
      self.packet_map[typ].append(packet)
    return packet


  # prints the parsed packet message (or doesn't if not verbose)
  def report(self, msg):
    if self.verbose:
      print(msg)


  # reads PR and LP packets
  def physics_packet_helper(self):
    if self.ignore_physics_packets:
      raise ParseError("Ignoring PR/LP packet (ignore_physics_packets = True)")
    flag = self.data.read(1)
    l0id = self.data.read(7)
    bcid = self.data.read(4)
    self.report("  TYP={} Flag={} L0ID={} BCID={}".format(self.typ.uint, flag,
      l0id.uint, bcid.uint))
    self.report("  ")
    last_cluster = BitArray("0x6FED", length=16)
    error_cluster = BitArray("0x77F4", length=16)
    clusters = []
    error_block = {}
    cluster_size = 0
    while True:
      cluster_size += 1
      if cluster_size > max_cluster_size:
        raise ParseError("Abort parsing PR/LP packet, exceeded the maximum cluster size")
      cluster = self.data.read(16)
      if cluster == last_cluster:
        self.report("  ")
        break
      elif cluster == error_cluster:
        abc_error = self.data.read(12)
        bcid_mismatch = self.data.read(12)
        l0tag_mismatch = self.data.read(12)
        input_timeout = self.data.read(12)
        error_block["abc_error"] = abc_error
        error_block["bcid_mismatch"] = bcid_mismatch
        error_block["l0tag_mismatch"] = l0tag_mismatch
        error_block["input_timeout"] = input_timeout
        self.report("  Error block:")
        self.report("    ABCStar Error {}".format(abc_error))
        self.report("    BCID Mismatch {}".format(bcid_mismatch))
        self.report("    L0tag Mismatch {}".format(l0tag_mismatch))
        self.report("    Input Channel Timeout {}".format(input_timeout))
      else:
        input_channel = cluster[1:5]
        cluster_address = cluster[5:13]
        next_strips_pattern = cluster[13:16]        
        self.report("  Input channel 0x{}".format(input_channel.hex))
        self.report("  Cluster address 0x{}".format(cluster_address.hex))
        self.report("  Next strips pattern {}".format(next_strips_pattern))
        self.report("  ")
        clusters.append({"channel" : input_channel,
          "address" : cluster_address, "pattern" : next_strips_pattern})
    packet = {"flag" : flag[0], "l0id" : l0id, "bcid" : bcid,
              "clusters" : clusters, "error_block" : error_block}    
    return packet


  # reads PR packets
  def parse_PR_packet(self):
    self.report("PR packet @ 0x{:x}({})".format(self.pos//8, (self.pos % 8) // 4))
    return self.physics_packet_helper()


  def parse_LP_packet(self):
    self.report("LP packet @ 0x{:x}({})".format(self.pos//8, (self.pos % 8) // 4))
    return self.physics_packet_helper()


  def abc_reg_read_helper(self, hpr=False):
    hcc_input_ch = self.data.read(4)
    reg_addr = self.data.read(8)
    if hpr and (reg_addr.uint != abc_hpr_addr):
      self.data.bitpos -= 12
      raise ParseError("Unexpected ABC* HPR address (expected {}, actual {})".format(
        abc_hpr_addr, reg_addr.uint))
    tbd = self.data.read(4)
    reg_data = self.data.read(32)
    status = self.data.read(16)
    tbd = self.data.read(4)
    self.report("  HCC Input Channel = {}".format(hcc_input_ch.uint))
    self.report("  Register Address = 0x{} (dec={})".format(reg_addr.hex,
                                              reg_addr.uint))
    self.report("  Register Data = 0x{}".format(reg_data.hex))
    self.report("  Status bits = 0x{}".format(status.hex))
    packet = {"hcc_channel" : hcc_input_ch.uint, "reg_addr" : reg_addr,
              "reg_data" : reg_data, "status" : status}
    return packet


  def hcc_reg_read_helper(self, hpr=False):
    reg_addr = self.data.read(8)  
    self.report("  Register Address = 0x{} (dec={})".format(reg_addr.hex, reg_addr.uint))
    if hpr and (reg_addr.uint != hcc_hpr_addr):
      self.data.bitpos -= 8
      raise ParseError("Unexpected HCC* HPR address (expected {}, actual {})".format(
        hcc_hpr_addr, reg_addr.uint))
    reg_data = self.data.read(32)
    tbd = self.data.read(4)
    self.report("  Register Data = 0x{}".format(reg_data.hex))
    packet = {"reg_addr" : reg_addr, "reg_data" : reg_data}
    return packet


  def parse_HCC_register_read(self):
    self.report("HCC register read packet @ 0x{:x}({})".format(self.pos//8, (self.pos % 8) // 4))
    return self.hcc_reg_read_helper()


  def parse_ABC_register_read(self):
    self.report("ABC register read packet @ 0x{:x}({})".format(self.pos//8, (self.pos % 8) // 4))
    return self.abc_reg_read_helper()


  def parse_ABC_high_priority(self):
    self.report("ABC high-priority register packet @ 0x{:x}({})".format(self.pos//8, (self.pos % 8) // 4))
    packet = self.abc_reg_read_helper(hpr=True)
    reg_data = packet['reg_data']
    adc_data = reg_data[20:32].hex
    lcb_s = reg_data[0:16].hex
    self.report("    LCB_locked = {}".format(reg_data[19])) # 12
    self.report("    LCB_raw_frame = 0x{}".format(lcb_s))
    self.report("    LCB_Decode_Err = {}".format(reg_data[18])) # 13
    self.report("    LCB_ErrCnt_Ovfl = {}".format(reg_data[17])) # 14
    self.report("    LCB_scmd_err = {}".format(reg_data[16]))  # 15
    self.report("    adc_data = 0x{}".format(adc_data))
    return packet


  def parse_HCC_high_priority(self):
    self.report("HCC high-priority register packet @ 0x{:x}({})".format(self.pos//8, (self.pos % 8) // 4))
    packet = self.hcc_reg_read_helper(hpr=True)
    reg_data = packet['reg_data']
    lcb_raw = reg_data[0:16].hex
    self.report("    R3L1_locked = {}".format(reg_data[31])) #0
    self.report("    lcb_locked = {}".format(reg_data[30])) #1
    self.report("    LCB_raw_frame = 0x{}".format(lcb_raw))   
    self.report("    lcb_decode_err = {}".format(reg_data[29])) #2
    self.report("    lcb_errcount_ovfl = {}".format(reg_data[28])) #3
    self.report("    lcb_scmd_err = {}".format(reg_data[27])) #4
    self.report("    ePllInstantLock = {}".format(reg_data[26])) #5
    self.report("    R3L1 errcount ovfl = {}".format(reg_data[25])) #6      
    return packet


  def parse_ABC_packet_transparent(self):
    raw_data = self.data.read(64)
    padding = self.data.read(4)
    self.report("ABC transparent packet @ 0x{:x}({}): {}".format(self.pos // 8, (self.pos % 8) // 4, raw_data.hex))
    packet = {"raw" : raw_data}
    return packet

  def parse_ABC_full_transparent(self):
    raise ParseError("Undocumented Full Transparent Mode packet @ 0x{:x}({})".format(self.pos//8, (self.pos % 8) // 4))

