from .parser import Parser as HCCParser
from .parser import TYP
from .driver_ftools import FtoolsBase

from abc import ABC, abstractmethod
from bitstring import BitArray

import os
import math

class HCCStarDriverBase(FtoolsBase, ABC):
  def __init__(self, dry_run=False, target="HCC", 
    elink_LCB="00", elink_R3_L1="00", elink_dout="00",
    hcc_id="0b1111", abc_id="0b1111", felix_device="0", path_to_ftools="",
    verbose=0, bc=0, fdaq_duration=2, nibble_order_from_host="inverse",
    nibble_order_to_host="direct", raise_on_error=True, delay_before_send=1,
    reset=False, ignore_physics_packets=True, descriptor=0):

    super().__init__() 

    self.dry_run = dry_run
    self.target = target
    self.elink_LCB = elink_LCB
    self.elink_dout = elink_dout
    self.elink_R3_L1 = elink_R3_L1
    self.elink_in = elink_dout
    self.elink_out = elink_LCB
    self.hcc_id = hcc_id
    self.abc_id = abc_id
    self.felix_device = felix_device
    self.path_to_ftools = path_to_ftools
    self.verbose = verbose
    self.fdaq_duration = fdaq_duration
    self.bc = bc
    self.nibble_order_from_host = nibble_order_from_host
    self.nibble_order_to_host = nibble_order_to_host
    self.raise_on_error = raise_on_error
    self.delay_before_send = delay_before_send
    self.reset = reset # reset elink when running fdaq
    self.ignore_physics_packets = ignore_physics_packets # don't parse physics packets
    self.descriptor = descriptor

    self.report_data = ""
    self.FNULL = open(os.devnull, 'w')
    self.hcc_parser = None
    self.fcheck_error = True
    self.do_parse_response = True

    self.check_regmap_version()

  #---------------------Abstract methods-------------------------
  # These methods define the details of the command encoding implementation 
  # for your particular firmware version

  # Define these methods in a child class, add class instantiation into
  # driver.py, and your firmware is supported by all tools in this package

  # The types of the parameters are:
  # hcc_id - integer 0..15 (4 bit)
  # abc_id - integer 0..15 (4 bit)
  # register_id - integer 0..255 (8 bit)
  # register_data - integer 0..4294967295 (32 bit)
  # target - string "ABC" or "HCC"
  # bc - integer 0..3 (2 bit)
  # bcr - integer 0..1 (1 bit)
  # mask (l0a) - integer 0..15 (4 bit)
  # tag - integer 0..63 (7 bit)
  # command_id - integer 0..15 (4 bit)

  # the return value is a BitArray or a list of BitArray's

  @abstractmethod
  def generate_write_command(self, hcc_id, abc_id, register_id, 
    register_data, target):
    pass

  @abstractmethod
  def generate_read_command(self, hcc_id, abc_id, register_id, target):
    pass

  @abstractmethod
  def generate_reset_command(self, bc):
    pass

  @abstractmethod
  def generate_l0a_command(self, bcr, mask, tag):
    pass

  @abstractmethod
  def generate_fast_command(self, bc, command_id):
    pass

  @abstractmethod
  def generate_r3l1_command(self, mask, tag):
    pass

  @abstractmethod
  def generate_idle_command(self):
    pass

  # ------- experimental commands -----

  @abstractmethod
  def generate_block_read(self, hcc_id, abc_id, register_id, 
    block_length, target):
    pass


  # data is an array
  @abstractmethod
  def generate_block_write(self, hcc_id, abc_id, register_id, 
    block_length, register_data, target):
    pass

  @abstractmethod
  def generate_read_write(self, hcc_id, abc_id, register_id, 
    register_data, target):
    pass

  @abstractmethod
  def generate_write_read(self, hcc_id, abc_id, register_id, 
    register_data, target):
    pass

  @abstractmethod
  def generate_block_read_write(self, hcc_id, abc_id, register_id,
    block_length, register_data, target):
    pass


  # data is an array
  @abstractmethod
  def generate_block_write_read(self, hcc_id, abc_id, register_id, 
    block_length, register_data, target):
    pass

  #------------------end of abstract methods----------------------

  # send IDLE packet
  def send_idle(self, destination="LCB"):
    command = self.generate_idle_command()
    if not command:
      raise ValueError("Driver returned an empty IDLE packet")
    self.send_command(command)


  # Send a fast command given a command ID
  def fast_command(self, command_id):    
    self.clear_report()
    bc = self.to_integer(self.bc)
    self.report("Fast command {} (bc={})".format(command_id, bc), verbosity=2)
    command_id = self.to_integer(command_id)
    command = self.generate_fast_command(bc, command_id)
    if not command:
      raise ValueError("Driver returned an empty fast command")
    self.send_command(command)


  # send L0A packet
  def L0A(self, bcr, mask, tag):    
    self.clear_report()
    bcr = int(bcr)
    mask = self.to_integer(mask)
    tag = self.to_integer(tag)
    command = self.generate_l0a_command(bcr, mask, tag)
    self.report("L0A command (bcr={}, mask=0x{:x}, tag=0x{:x})".format(
      bcr, mask, tag), verbosity=2)
    if not command:
      raise ValueError("Driver returned an empty L0A command")
    self.send_command(command)

  # send L0A packet
  def L0A_and_listen(self, bcr, mask, tag):    
    self.clear_report()
    bcr = int(bcr)
    mask = self.to_integer(mask)
    tag = self.to_integer(tag)
    self.report("L0A command (bcr={}, mask=0x{:x}, tag=0x{:x}) + listen".format(
      bcr, mask, tag), verbosity=2)
    command = self.generate_l0a_command(bcr, mask, tag)
    if not command:
      raise ValueError("Driver returned an empty L0A command")
    self.listen_and_send_command(command)


  # send R3L1 command
  def R3L1(self, mask, tag):    
    self.clear_report()
    mask = self.to_integer(mask)
    tag = self.to_integer(tag)
    command = self.generate_r3l1_command(mask, tag)
    self.report("R3L1 command (mask=0x{:x}, tag=0x{:x})".format(mask, tag), verbosity=2)
    if not command:
      raise ValueError("Driver returned an empty R3L1 command")
    self.send_command(command)


  # Send reset sequence
  def full_reset(self):
    self.report("Reset command sequence", verbosity=2)
    self.clear_report()
    bc = self.to_integer(self.bc)
    command = self.generate_reset_command(bc)
    if not command:
      raise ValueError("Driver returned an empty reset command")
    self.send_command(command)

  # Read a register given its ID (HCC* or ABC*)
  # (target="HCC" or "ABC"; if not provided the value is taken from the class)
  def read_register(self, register_id, target=None, hcc_id=None, abc_id=None):    
    target = target or self.target
    hcc_id = hcc_id or self.hcc_id
    abc_id = abc_id or self.abc_id
    hcc_id = self.to_integer(hcc_id)
    abc_id = self.to_integer(abc_id)
    register_id = self.to_integer(register_id)
    self.report("Read register 0x{:x} (target={}, hcc_id=0x{:x}, abc_id={:x})".format(
      register_id, target, hcc_id, abc_id), verbosity=2)
    self.clear_report()
    command = self.generate_read_command(hcc_id, abc_id, register_id, target)
    if not command:
      raise ValueError("Driver returned an empty read command")
    self.listen_and_send_command(command) 


  # Write a register given its ID and data (HCC* or ABC*)
  # (target="HCC" or "ABC"; if not provided the value is taken from the class)
  def write_register(self, register_id, register_data, target=None, 
    hcc_id=None, listen=False, abc_id=None, generator=None):
    self.report("Write register command", verbosity=2)
    target = target or self.target
    hcc_id = hcc_id or self.hcc_id
    abc_id = abc_id or self.abc_id    
    hcc_id = self.to_integer(hcc_id)
    abc_id = self.to_integer(abc_id)
    register_id = self.to_integer(register_id)
    register_data = self.to_integer(register_data)
    generator = generator or self.generate_write_command
    self.report("Write 0x{:08x} to register 0x{:x} (target={}, hcc_id=0x{:x}, abc_id={:x})".format(
      register_data, register_id, target, hcc_id, abc_id), verbosity=1)
    self.clear_report()
    command = generator(hcc_id=hcc_id, abc_id=abc_id, register_id=register_id,
      register_data=register_data, target=target)
    if not command:
      raise ValueError("Driver returned an empty write command")
    if listen:
      self.listen_and_send_command(command)
    else:
      self.send_command(command)


  # Receive the data from the chip without sending anything
  def listen(self):
    self.report("Capture data from HCC*/ABC*", verbosity=2)
    self.clear_report()
    self.listen_and_send_command(None)


  # send ABC* register reset
  def abc_register_reset(self):
    self.report("ABC* register reset", verbosity=2)
    self.fast_command(3)


  # send HCC* register reset
  def hcc_register_reset(self):
    self.report("HCC* register reset", verbosity=2)
    self.fast_command(12)


  def set_hcc_id(self, hcc_id, serial):
    serial = self.to_integer(serial)
    hcc_id = self.to_integer(hcc_id)
    self.report("Set HCC* ID=0x{:x}, serial=0x{:x}".format(hcc_id, serial),
      verbosity=2)    
    self.write_register(16, 0b100, target="HCC", hcc_id="0b1111")
    value = hcc_id * 2**28 + serial
    self.write_register(17, value, target="HCC", hcc_id="0b1111")


  # Default configuration for HCC* to enable ABC* chips
  def enable_ABC_star(self):
    self.report("Configure HCC* to enable ABC* chips", verbosity=1) 
    self.write_register(32, 0x02000000, target="HCC")
    self.write_register(33, 0x40000000, target="HCC")
    self.write_register(34, 0x40, target="HCC")
    self.write_register(35, 0x00FF3B05, target="HCC")
    self.write_register(36, 0, target="HCC")
    self.write_register(37, 0x4, target="HCC")
    self.write_register(38, 0x0FFFFFFF, target="HCC")
    self.write_register(39, 0x00000014, target="HCC")
    self.write_register(40, 0x7ff, target="HCC")
    self.write_register(44, 1, target="HCC")
    self.write_register(45, 1, target="HCC")
    self.write_register(46, 1, target="HCC")


  # Default configuration for ABC* chips
  def configure_ABC_star(self): 
    self.report("Configure ABC* registers", verbosity=1)
    # ANALOG AND DCS REGISTERS   
    self.fast_command(3)
    self.write_register(1, 0x060D0F0D, target="ABC")
    self.write_register(2, 0x100F100D, target="ABC")
    self.write_register(3, 0x00000D02, target="ABC")
    self.write_register(4, 0x0000000C, target="ABC")
    self.write_register(6, 0x0002000C, target="ABC")
    self.write_register(7, 0x80070000, target="ABC")
    # INPUT (MASK) REGISTERS
    self.write_register(16, 0x12345678, target="ABC")
    # CONFIGURATION REGISTER
    self.write_register(32, 0x0ff50770, target="ABC")
    self.write_register(33, 0x00000004, target="ABC")
    self.write_register(34, 0x41800003, target="ABC")
    self.write_register(35, 0x00042000, target="ABC")
    self.write_register(36, 0x00000000, target="ABC")
    self.write_register(38, 0x00000086, target="ABC")

  # experimental
  # Read a register block given its ID (HCC* or ABC*)
  # (target="HCC" or "ABC"; if not provided the value is taken from the class)
  def read_register_block(self, register_id, block_length=1,
    target=None, hcc_id=None, abc_id=None):    

    target = target or self.target
    hcc_id = hcc_id or self.hcc_id
    abc_id = abc_id or self.abc_id
    hcc_id = self.to_integer(hcc_id)
    abc_id = self.to_integer(abc_id)
    block_length = self.to_integer(block_length)
    register_id = self.to_integer(register_id)
    self.report(("Read register block 0x{:x} (target={}, hcc_id=0x{:x},"
      + " abc_id={:x}, block_length={})").format(register_id, target, hcc_id,
      abc_id, block_length), verbosity=2)
    self.clear_report()
    command = self.generate_block_read(hcc_id=hcc_id, abc_id=abc_id,
      register_id=register_id, block_length=block_length, target=target)
    if not command:
      raise ValueError("Driver returned an empty read_register_block command")
    self.listen_and_send_command(command) 


  # experimental
  # Write a register given its ID and data (HCC* or ABC*)
  # (target="HCC" or "ABC"; if not provided the value is taken from the class)
  def write_register_block(self, register_id, register_data, block_length=1,
    target=None, hcc_id=None, listen=False, abc_id=None, generator=None):

    self.report("Write register block command", verbosity=2)
    target = target or self.target
    hcc_id = hcc_id or self.hcc_id
    abc_id = abc_id or self.abc_id
    hcc_id = self.to_integer(hcc_id)
    abc_id = self.to_integer(abc_id)
    generator = generator or self.generate_block_write
    block_length = self.to_integer(block_length)
    register_id = self.to_integer(register_id)
    data = [self.to_integer(x) for x in register_data]
    self.report("Write {} to registers {} (target={}, hcc_id=0x{:x}, abc_id={:x})".format(
      register_data, 
      ["0x{:x}".format(register_id + i) for i in range(len(register_data))],
      target, hcc_id, abc_id), verbosity=1)
    self.clear_report()
    command = generator(hcc_id=hcc_id, abc_id=abc_id, register_id=register_id,
      register_data=data, block_length=block_length, target=target)
    if not command:
      raise ValueError("Driver returned an empty write_register_block command")
    if listen:
      self.listen_and_send_command(command)
    else:
      self.send_command(command)


  def report_parser_status(self):
    if self.fcheck_error:
      print("FELIX data contains error blocks!")
      print("Please run \"fcheck {}\" to inspect the blocks".format(self.fdaq_file))
    errors = self.hcc_parser.parse_errors
    max_parse_time = self.hcc_parser.max_parse_time
    self.report(("Found {} valid packets while parsing HCC*/ABC* response, encountered "
      + "{} parsing errors\n").format(self.hcc_parser.packets_total, errors))
    if errors != 0:
      print("Encountered {} parsing errors in HCC*/ABC* response data".format(errors))
      print("Please run \"./parse_file.py dataout.dat | less\" to inspect the data")
    if self.hcc_parser.parse_timeout:
      print("HCC*/ABC* response parser timed out ({}s parsing time allowed)".format(max_parse_time))


  # Decode and report data from the last HCC* HPR packet
  def report_hcc_status(self):
    self.report("HCC* status:")
    if self.hcc_parser.packet_map[TYP.HCC_high_priority]:
      reg_data = self.hcc_parser.packet_map[TYP.HCC_high_priority][-1]['reg_data']
      lcb_raw = reg_data[0:16].hex
      reg_data.reverse()
      self.report("R3L1_locked = {}".format(reg_data[0]))
      self.report("lcb_locked = {}".format(reg_data[1]))
      self.report("LCB_raw_frame = 0x{}".format(lcb_raw))   
      self.report("lcb_decode_err = {}".format(reg_data[2]))
      self.report("lcb_errcount_ovfl = {}".format(reg_data[3]))
      self.report("lcb_scmd_err = {}".format(reg_data[4]))
      self.report("ePllInstantLock = {}".format(reg_data[5]))
      self.report("R3L1 errcount ovfl = {}".format(reg_data[6]))      
    else:
      self.report("(N/A - no HPR packets received)")  
    self.report(" ")


  # Decode and report data from the last ABC* HPR packet
  def report_abc_status(self):
    self.report("ABC* status:")
    if self.hcc_parser.packet_map[TYP.ABC_high_priority]:
      reg_data = self.hcc_parser.packet_map[TYP.ABC_high_priority][-1]['reg_data']
      adc_data = reg_data[20:32].hex
      lcb_s = reg_data[0:16].hex
      reg_data.reverse()
      self.report("LCB_locked = {}".format(reg_data[12]))
      self.report("LCB_raw_frame = 0x{}".format(lcb_s))
      self.report("LCB_Decode_Err = {}".format(reg_data[13]))
      self.report("LCB_ErrCnt_Ovfl = {}".format(reg_data[14]))
      self.report("LCB_scmd_err = {}".format(reg_data[15]))      
      self.report("adc_data = 0x{}".format(adc_data))
    else:
      self.report("(N/A - no HPR packets received)")
    self.report(" ")   


  def report_physics_helper(self, packets):
    for packet in packets:
      self.report("Flag={} L0ID={} BCID={}".format(packet["flag"],
        packet["l0id"], packet["bcid"]))
      for cluster in packet["clusters"]:
        self.report("  Cluster data: channel: 0x{}, address: 0x{}, pattern: {}".format(
          cluster["channel"].hex, cluster["address"].hex, cluster["pattern"]))
      error_block = packet["error_block"]
      self.report("  Error block: ABCStar error={}, BCID mismatch={}, L0tag mismatch = {}, Input channel timeout = {}".format(
        error_block["abc_error"], error_block["bcid_mismatch"], 
        error_block["l0tag_mismatch"], error_block["input_timeout"]))


  def report_physics_packets(self):    
    if self.hcc_parser.packet_map[TYP.LP]:
      self.report("LP packets:")
      self.report_physics_helper(self.hcc_parser.packet_map[TYP.LP])
    else:
      self.report("LP packets: (none)")

    if self.hcc_parser.packet_map[TYP.PR]:
      self.report("PR packets:")
      self.report_physics_helper(self.hcc_parser.packet_map[TYP.PR])
    else:
      self.report("PR packets: (none)")


  # parse the data again, look for a specific register read
  def find_register_read(self, register_id, target=None):
    register_id = self.to_integer(register_id)
    if self.no_data_received_from_elink():
      raise Exception("No data was received from FELIX device {} elink {}".format(
        self.felix_device, self.elink_dout))
    swp_nbl = self.nibble_order_to_host != "direct"
    data = BitArray(filename=self.data_filename)
    parser = HCCParser(data, swap_nibbles=swp_nbl,
      ignore_physics_packets=self.ignore_physics_packets)
    packet = parser.find_register_read(register_id, target=target)
    return [packet]

  def parse_response(self, filename=None):
    if not self.do_parse_response:
      return
      
    if filename is None: filename = self.data_filename 

    self.report("Parsing HCC*/ABC* data contained in file {}".format(filename), verbosity=2)
    if self.dry_run: return
    swp_nbl = self.nibble_order_to_host != "direct"

    if self.no_data_received_from_elink():
      self.hcc_parser = HCCParser(None, swap_nibbles=swp_nbl, 
        ignore_physics_packets=self.ignore_physics_packets)
    else:
      data = BitArray(filename=filename)
      self.hcc_parser = HCCParser(data, swap_nibbles=swp_nbl, 
        ignore_physics_packets=self.ignore_physics_packets)
      self.hcc_parser.parse()
    self.report(self.hcc_parser.packet_map, verbosity=3)

  # returns the target if (ABC or HCC)
  def get_target_select(self, target):
    HCC = 'HCC'
    ABC = 'ABC'
    valid_targets = [HCC, ABC]
    if not target in valid_targets:
      raise Exception("Invalid target. Valid targets:" + str(valid_targets))
    return '0b0' if target == 'HCC' else '0b1'


  def to_integer(self, arg):
    if isinstance(arg, int):
      return arg
    return int(arg, 0)



  def to_float(self, arg):
    if isinstance(arg, float):
      return arg
    return float(arg)