from .codec import Codec6b8b
from .codec import swap_nibbles
from .driver_base import HCCStarDriverBase

from bitstring import BitArray, BitStream
from bidict import bidict


k2 = BitArray('0x47', length=8)
k3 = BitArray('0x6A', length=8)

# Handles the HCC* communication protocol -- encoding and
# frame formatting is implemented in software
def get_driver(base_class):

  class HCCStarDriverSoftware(base_class):

    def generate_fast_command(self, bc, command_id):
      packet = self.fast_command_packet(bc, command_id)  
      return self.encode_command(packet, self.encode_fast_command)


    def generate_l0a_command(self, bcr, mask, tag): 
      packet = (BitArray(uint=bcr, length=1) + BitArray(uint=mask, length=4)
              + BitArray(uint=tag, length=7))
      return self.encode_command(packet, self.encode_L0A)


    def generate_reset_command(self, bc):
      packets = packets = [self.fast_command_packet(bc, 3),
        self.fast_command_packet(bc, 2),
        self.fast_command_packet(bc, 12)]
      return self.encode_command(packets, self.encode_fast_sequence)


    def generate_read_command(self, hcc_id, abc_id, register_id, target):
      packets = self.read_register_packets(hcc_id, abc_id, register_id, target)
      return self.encode_command(packets, self.encode_register_rw)


    def generate_write_command(self, hcc_id, abc_id, register_id, 
      register_data, target):
      packets = self.write_register_packets(hcc_id, abc_id, register_id,
                register_data, target)
      return self.encode_command(packets, self.encode_register_rw)
    

    def generate_r3l1_command(self, mask, tag):
      packet = BitArray(uint=mask, length=5) + BitArray(uint=tag, length=7)
      return self.encode_command(packet, self.encode_L0A)


    def generate_idle_command(self):    
      return BitArray('0x7855')


  # emulate experimental commands
  # main use is testing, like preparing testbench data
    def generate_block_read(self, hcc_id, abc_id, register_id, 
      block_length, target):
      commands = [self.generate_read_command(hcc_id=hcc_id, abc_id=abc_id, 
        register_id=(register_id + i), target=target) for i in range(block_length)]
      return BitArray().join(commands)


    def generate_block_write(self, hcc_id, abc_id, register_id, 
      block_length, register_data, target):
      commands = [self.generate_write_command(hcc_id=hcc_id, abc_id=abc_id, 
        register_id=(register_id + i), register_data=register_data[i],
        target=target) for i in range(block_length)]
      return BitArray().join(commands)


    def generate_read_write(self, hcc_id, abc_id, register_id, 
      register_data, target):
      return BitArray().join([
        self.generate_read_command(hcc_id=hcc_id, abc_id=abc_id, 
          register_id=register_id, target=target),
        self.generate_write_command(hcc_id=hcc_id, abc_id=abc_id, 
          register_id=register_id, register_data=register_data, target=target)
        ])


    def generate_write_read(self, hcc_id, abc_id, register_id, 
        register_data, target):
      return BitArray().join([
        self.generate_write_command(hcc_id=hcc_id, abc_id=abc_id, 
          register_id=register_id, register_data=register_data, target=target),
        self.generate_read_command(hcc_id=hcc_id, abc_id=abc_id, 
          register_id=register_id, target=target)
        ])


    def generate_block_read_write_helper(self, hcc_id, abc_id, register_id,
        block_length, register_data, target):
      read_commands = [self.generate_read_command(hcc_id=hcc_id, abc_id=abc_id, 
        register_id=(register_id + i), target=target) for i in range(block_length)]
      write_commands = [self.generate_write_command(hcc_id=hcc_id, abc_id=abc_id, 
        register_id=(register_id + i), register_data=register_data[i],        
        target=target) for i in range(block_length)]
      return (read_commands, write_commands)


    def generate_block_read_write(self, hcc_id, abc_id, register_id,
        block_length, register_data, target):
      (read_commands, write_commands) = self.generate_block_read_write_helper(
        hcc_id, abc_id, register_id, block_length, register_data, target)
      result = []
      for i in range(block_length):
        result.append(read_commands[i])
        result.append(write_commands[i])
      result = BitArray().join(result)
      return result


    def generate_block_write_read(self, hcc_id, abc_id, register_id, 
      block_length, register_data, target):
      (read_commands, write_commands) = self.generate_block_read_write_helper(
        hcc_id, abc_id, register_id, block_length, register_data, target)
      result = []
      for i in range(block_length):
        result.append(write_commands[i])
        result.append(read_commands[i])
      result = BitArray().join(result)
      return result

    # ========================= private methods below ===============================

    # encodes a packets into a command
    def encode_command(self, packets, encoder_function):
      command = encoder_function(packets)
      self.report("Before encoding:", verbosity=2)
      if isinstance(packets, list):
        [self.report(packet.bin, verbosity=2) for packet in packets]
      return command


    # returns start or stop packet with given parameters
    # for register read/write commands
    def frame_packet(self, HCC_id, target_select, start_stop):
      return (BitArray(target_select) + BitArray(start_stop)
              + BitArray(uint=HCC_id, length=4))


    # returns the first two packets with the register address 
    # for register read and write commands
    def register_rw_header_packets(self, ABC_id, register_id, rw):
      spare = BitArray('0b0')
      marker = BitArray(uint=0, length=5)
      register_bits = BitArray(uint=register_id, length=8)
      header_packet_1 = (marker + rw + BitArray(uint=ABC_id, length=4)
                       + register_bits[0:2])
      header_packet_2 = (marker + register_bits[2:8] + spare)
      return [header_packet_1, header_packet_2]


    # Create an array of unencoded packets for reading a given register
    def read_register_packets(self, HCC_id, ABC_id, register_id, target):
      rw = BitArray('0b1')
      start = BitArray('0b1')
      stop = BitArray('0b0')
      target_select = self.get_target_select(target)
      start_packet = self.frame_packet(HCC_id, target_select, start)
      stop_packet = self.frame_packet(HCC_id, target_select, stop)
      header_packets = self.register_rw_header_packets(ABC_id, register_id, rw)
      return [start_packet, header_packets[0], header_packets[1], stop_packet]


    # Create an array of unencoded packets for writing value to a given register
    def write_register_packets(self, HCC_id, ABC_id, register_id, value, target):
      rw = BitArray('0b0')
      start = BitArray('0b1')
      stop = BitArray('0b0')
      marker = BitArray(uint=0, length=5)
      value_bits = BitArray(uint=value, length=32)
      value_bits.reverse()
      target_select = self.get_target_select(target)
      start_packet = self.frame_packet(HCC_id, target_select, start)
      stop_packet = self.frame_packet(HCC_id, target_select, stop)
      header_packets = self.register_rw_header_packets(ABC_id, register_id, rw)
      write_packet_1 = marker + BitArray('0b000') + value_bits[31:27:-1]
      write_packet_2 = marker + value_bits[27:20:-1]
      write_packet_3 = marker + value_bits[20:13:-1]
      write_packet_4 = marker + value_bits[13:6:-1]
      write_packet_5 = marker + value_bits[6::-1]
      return [start_packet, header_packets[0], header_packets[1], write_packet_1,
              write_packet_2, write_packet_3, write_packet_4, write_packet_5,
              stop_packet]


    # returns encoded version of the given fast command
    def encode_L0A(self, packet):
      output = Codec6b8b.encode_array(packet)
      if self.nibble_order_from_host == "inverse":
        self.report("L0A - swapping nibbles (from host)")
        return swap_nibbles(output)
      else:
        return output


    # Create an array of unencoded packets for a fast command
    def fast_command_packet(self, bc, command):
      return BitArray(uint=bc, length=2) + BitArray(uint=command, length=4)


    # returns encoded version of the given fast command
    def encode_fast_command(self, packet):
      encoded = Codec6b8b.encode_word(packet)
      output = k3 + encoded
      if self.nibble_order_from_host == "inverse":
        self.report("Fast command - swapping nibbles (from host)")
        return swap_nibbles(output)
      else:
        return output


    # encode a fast command sequence
    def encode_fast_sequence(self, packets):
      commands = [self.encode_fast_command(packet) for packet in packets]
      return BitArray().join(commands)


    # encodes register r/w command to 6b8b encoding
    # (and swapps nibbles if necessary)
    def encode_register_rw(self, packets):
      encoded = [Codec6b8b.encode_array(packet) for packet in packets]
      output = (k2 + encoded[0] + BitArray().join(encoded[1:-1]) + k2 + encoded[-1])
      if self.nibble_order_from_host == "inverse":
        self.report("Register command - swapping nibbles (from host)")
        return swap_nibbles(output)
      else:
        return output

  return HCCStarDriverSoftware