#!/bin/bash

# Assign HCC_IDs to the stave (lpGBT master only)

REGMAP=`flx-config get REG_MAP_VERSION | sed 's/REG_MAP_VERSION=0x//' | cut -c 1-1`

if [ $REGMAP -eq 5 ]
then
	# phase2 firmware
	ELINKS=("010" "010" "010" "010" "006" "006" "006" "006" "00b" "00b" "00b" "00b" "00b" "001")
else    
	# phase1 firmware
	ELINKS=("01e" "01e" "01e" "01e" "01a" "01a" "01a" "01a" "01c" "01c" "01c" "01c" "01c" "018")
fi

HCC_IDS=("0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13")
EFUSES=("0x000000" "0x000000" "0x001a4e" "0x004224" "0x004223" "0x001a4f" "0x003a46" "0x003a45" "0x001a22" "0x001a21" "0x001a33" "0x001a3b" "0x001a3c" "0x001a4c")

int_handler()
{
    printf "\e[0m\n"
    echo "Ctrl+C"
    printf "\n"
    kill $PPID
    exit 1
}
trap 'int_handler' INT

echo "Assign HCC_IDs on the master stave"
for i in `seq 0 13`;
do
	echo "Assign HCC_ID=${HCC_IDS[$i]} to chip with eFuseID=${EFUSES[$i]} (TX elink ${ELINKS[$i]})"
	echo "    ./hcc_tool.py set_hcc_id -hi ${HCC_IDS[$i]} -se ${EFUSES[$i]} -e_lcb ${ELINKS[$i]} -nfh direct"
	./hcc_tool.py set_hcc_id -hi ${HCC_IDS[$i]} -se ${EFUSES[$i]} -e_lcb ${ELINKS[$i]} -nfh direct
done
