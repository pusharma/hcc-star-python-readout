#!/usr/bin/env python3

from hcc_star.parser import Parser as HCCParser
from hcc_star.parser import TYP
from hcc_star.codec import swap_nibbles

import argparse
from bitstring import BitArray
import os
import subprocess
from time import sleep
from enum import IntEnum
from signal import signal, SIGPIPE, SIG_DFL

__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"

def main():
  signal(SIGPIPE, SIG_DFL)
  parser = argparse.ArgumentParser(description="Utility for reading data from HCCStar. The data has to be stripped from FELIX header and trailer.")
  parser.add_argument('file_name', default="dataout.dat", help='file to parse', type=str)
  parser.add_argument('-ipp', '--ignore_physics_packets', help='Parser skips physics packets (use if the data coming from HCC*/ABC* is heavily corrupt)', action="store_true")

  global args
  args = parser.parse_args()

  print("Loading {}".format(args.file_name))
  data = BitArray(filename=args.file_name)
  data = data

  hcc_parser = HCCParser(data, verbose=True, max_parse_time=-1, ignore_physics_packets=args.ignore_physics_packets)
  hcc_parser.parse()


if __name__ == '__main__':
    main()
