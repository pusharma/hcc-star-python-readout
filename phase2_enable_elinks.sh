#!/bin/bash

echo "Configure and enable AMAC elinks (link 0)"
# direct mode ToHost, FromHost
# encoding configuration is ignored by AMAC elinks
flx-config set MINI_EGROUP_TOHOST_00_EC_ENCODING=0
flx-config set MINI_EGROUP_FROMHOST_00_EC_ENCODING=0
# turn on the EC elinks
flx-config set MINI_EGROUP_TOHOST_00_EC_ENABLE=1
flx-config set MINI_EGROUP_FROMHOST_00_EC_ENABLE=1
# turn on IC elinks
flx-config set MINI_EGROUP_TOHOST_00_IC_ENABLE=1
flx-config set MINI_EGROUP_FROMHOST_00_IC_ENABLE=1

echo "Configure and enable AMAC elinks (link 1)"
# direct mode ToHost, FromHost
# encoding configuration is ignored by AMAC elinks
flx-config set MINI_EGROUP_TOHOST_01_EC_ENCODING=0
flx-config set MINI_EGROUP_FROMHOST_01_EC_ENCODING=0
# turn on the EC elinks
flx-config set MINI_EGROUP_TOHOST_01_EC_ENABLE=1
flx-config set MINI_EGROUP_FROMHOST_01_EC_ENABLE=1
# turn on IC elinks
flx-config set MINI_EGROUP_TOHOST_01_IC_ENABLE=1
flx-config set MINI_EGROUP_FROMHOST_01_IC_ENABLE=1

# todo: loop over 7 egroups, 2 links
echo "Configure decoding elinks (lpGBT link 0)"
# phase2 decoder is currently hardcoded as 8b10b, 320 Mbps
# select 8 bit width
# flx-config set DECODING_LINK00_EGROUP0_CTRL_EPATH_WIDTH=2
# select 8b10b
# flx-config set DECODING_LINK00_EGROUP0_CTRL_PATH_ENCODING=1

# turn on all decoding elinks
flx-config set DECODING_LINK00_EGROUP0_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK00_EGROUP0_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK00_EGROUP1_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK00_EGROUP1_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK00_EGROUP2_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK00_EGROUP2_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK00_EGROUP3_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK00_EGROUP3_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK00_EGROUP4_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK00_EGROUP4_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK00_EGROUP5_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK00_EGROUP5_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK00_EGROUP6_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK00_EGROUP6_CTRL_EPATH_WIDTH=0x02

echo "Configure decoding elinks (lpGBT link 1)"
flx-config set DECODING_LINK01_EGROUP0_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK01_EGROUP0_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK01_EGROUP1_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK01_EGROUP1_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK01_EGROUP2_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK01_EGROUP2_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK01_EGROUP3_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK01_EGROUP3_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK01_EGROUP4_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK01_EGROUP4_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK01_EGROUP5_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK01_EGROUP5_CTRL_EPATH_WIDTH=0x02
flx-config set DECODING_LINK01_EGROUP6_CTRL_EPATH_ENA=0xff
flx-config set DECODING_LINK01_EGROUP6_CTRL_EPATH_WIDTH=0x02

echo "Configure encoding elinks (lpGBT link 0)"
# phase2 decoder is currently hardcoded
# !!!set all encoding elinks to 2-bit wide if fupload complains
# select 4 bit width (configuration ignored by the Strips links, unnecessary?)
# flx-config set ENCODING_LINK00_EGROUP0_CTRL_EPATH_WIDTH=1
# select direct mode (ignored by the Strips link, unnecessary?)
# flx-config set ENCODING_LINK00_EGROUP0_CTRL_PATH_ENCODING=0

# turn on all encoding elinks
flx-config set ENCODING_LINK00_EGROUP0_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK00_EGROUP1_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK00_EGROUP2_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK00_EGROUP3_CTRL_EPATH_ENA=0x00

flx-config set ENCODING_LINK00_EGROUP0_CTRL_EPATH_ENA=0xff
flx-config set ENCODING_LINK00_EGROUP1_CTRL_EPATH_ENA=0xff
flx-config set ENCODING_LINK00_EGROUP2_CTRL_EPATH_ENA=0xff
flx-config set ENCODING_LINK00_EGROUP3_CTRL_EPATH_ENA=0xff

echo "Configure encoding elinks (lpGBT link 1)"
flx-config set ENCODING_LINK01_EGROUP0_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK01_EGROUP1_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK01_EGROUP2_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK01_EGROUP3_CTRL_EPATH_ENA=0x00

flx-config set ENCODING_LINK01_EGROUP0_CTRL_EPATH_ENA=0xff
flx-config set ENCODING_LINK01_EGROUP1_CTRL_EPATH_ENA=0xff
flx-config set ENCODING_LINK01_EGROUP2_CTRL_EPATH_ENA=0xff
flx-config set ENCODING_LINK01_EGROUP3_CTRL_EPATH_ENA=0xff

echo "Configure timeout"
flx-config set TIMEOUT_CTRL_ENABLE=1
flx-config set TIMEOUT_CTRL_TIMEOUT=0xffffffff
