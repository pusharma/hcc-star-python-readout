import re
import json

# these parser classes input register data in user-readable format and translate them to
# an array of hashmaps, containing information about each registar write.
#
# output format - an array of register data:
# [
# 	{
# 		"reg_id" 	: "2a",	 # all numbers are hex string lower case
# 		"type"		: "HCC",
# 		"name"		: "OpMode", # register name is optional
# 		"reg_data" 	: "00020001", 	
# 		"hcc_id"	: "f", 	
# 		"abc_id"	: "f" 	
# 	},
# 	....
# ]


# base class, don't instantiate
class ItsdaqSummary():

	# matches the data format in the itsdaq summary file
	pattern = re.compile("Register\s+(?P<regid>\d+):\s+(?P<regname>\S+)\s+(?P<regdata>(?:(?:\S+\s*))+)")

	# convert text file into an array of register definitions
	def parse(self, file_name):
		result = []
		
		with open(file_name) as f:
			lines = f.readlines()

		for line in lines:
			#print(line)
			if line[0] == "#":
				continue

			match = ItsdaqSummary.pattern.match(line)
			reg_id = int(match.group("regid"))
			register_data = match.group("regdata").split()

			for hcc_id, data in enumerate(register_data):
				command_data = {}
				command_data["reg_id"] = "{:x}".format(reg_id)
				command_data["name"] = match.group("regname")
				command_data["type"] = self.register_type							
				command_data["reg_data"] = data.lower()
				command_data["hcc_id"] = "{:x}".format(hcc_id)				
				command_data["abc_id"] = "f"

				result.append(command_data)

		#print(json.dumps(result, indent=4))
		return result


class ItsdaqSummaryABC(ItsdaqSummary):
	def __init__(self):
		self.register_type = "ABC"


class ItsdaqSummaryHCC(ItsdaqSummary):
	def __init__(self):
		self.register_type = "HCC"


# base class, don't instantiate
class ItsdaqSummaryBroadcast():
	# matches the data format in the itsdaq summary file (only one HCC* chip)
	pattern = re.compile("Register\s+(?P<regid>\d+):\s+(?P<regname>\S+)\s+(?P<regdata>(?:\S+)\s*)")

	# convert text file into an array of register definitions
	def parse(self, file_name):
		result = []
		
		with open(file_name) as f:
			lines = f.readlines()

		for line in lines:
			#print(line)
			if line[0] == "#":
				continue

			match = ItsdaqSummaryBroadcast.pattern.match(line)
			reg_id = int(match.group("regid"))
			command_data = {}			
			command_data["reg_id"] = "{:x}".format(reg_id)
			command_data["name"] = match.group("regname")
			command_data["reg_data"] = match.group("regdata").lower()
			command_data["hcc_id"] = "f"
			command_data["abc_id"] = "f"
			command_data["type"] = self.register_type
			result.append(command_data)

		#print(json.dumps(result, indent=4))
		return result


class ItsdaqSummaryBroadcastABC(ItsdaqSummaryBroadcast):
	def __init__(self):
		self.register_type = "ABC"


class ItsdaqSummaryBroadcastHCC(ItsdaqSummaryBroadcast):
	def __init__(self):
		self.register_type = "HCC"


class JsonFile():
	def parse(self, file_name):
		with open(file_name, "r") as read_file:
			return json.load(read_file)
