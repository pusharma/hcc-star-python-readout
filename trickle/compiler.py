# converts trickle configuration into data format compatible with fupload

from os import path
import jstyleson
import importlib
from bitstring import BitArray, BitStream
from .encoder import Encoder

# build a command stream for the trickle configuration
class Compiler():
	__author__ = "Elena Zhivun"
	__version__ = "0.0.1"
	__maintainer__ = "Elena Zhivun"
	__email__ = "ezhivun@bnl.gov"


	def __init__(self, base_path):
		self.base_path = base_path
		self.trickle = [None]*4
		self.parsers = importlib.import_module("trickle.parsers")
		index_path = path.join(base_path, "index.json")

		with open(index_path) as f:
			self.index = jstyleson.load(f)
		self.segment_map = self.index["input"]["segment_map"]
		self.inputs = self.index["input"]["data"]
		self.outputs = self.index["output"]


	# add firmware commands into all selected segments
	def append_bitstream(self, fw_command, segments):		
		if not (type(segments) is list):
			segments = [segments]

		for segment in segments:
			if self.trickle[segment] is None:
				self.trickle[segment] = fw_command
			else:
				self.trickle[segment].append(fw_command)	


	# add commands from "before_all" section
	def process_commands_before(self, input_data):
		try:
			# find what actions must be done at the start of trickle configuration
			if not ("before_all" in input_data["config"]):
				actions_before_all = []
			else:
				actions_before_all = input_data["config"]["before_all"]				
			fw_command = BitArray().join(
					[self.encoder.do_action(action) for action in actions_before_all])

			# flatten the list of all segments and select each segment once
			segments = list(set(self.segment_map.values()))
			self.append_bitstream(fw_command, segments)

		except Exception as e:
			print("Error adding before_all commands")		
			raise(e)


	# add commands from "for_each" section
	def process_commands_each(self, commands, input_data):
		
		try:		
			for command in commands:
				# find which segment the configuration should be written to
				segments = self.segment_map[command["hcc_id"]]

				# find what actions must be done for each command
				if not ("for_each" in input_data["config"]):
					actions_for_each = ["RegisterWrite", "RegisterRead"]
				else:
					actions_for_each = input_data["config"]["for_each"]

				# encode and join all trickle commands
				fw_command = BitArray().join(
					[self.encoder.do_action(action, command=command) for action in actions_for_each])

				# add commands to trickle configuration
				self.append_bitstream(fw_command, segments)

				# print("Encoding register {}, hcc_id=0x{:x} (segment {}), abc_id=0x{:x} data=0x{}".format(
				# reg_id, hcc_id, segment, abc_id, reg_data))			
				# print(fw_command.hex)
				# input("Press Enter to continue... ")

		except Exception as e:
			print("Error encoding command (for_each section):")
			print(jstyleson.dumps(command, indent=4))			
			raise(e)

		# return trickle


	# save the data for fupload to send trickle configuration and return the number of bytes written
	def save_hex(self, data, file_name):
		byte_count = 0
		with open(file_name, 'w') as f:
			if data is None:
				return byte_count
			for byte in data.cut(8):
				f.write("0x{:02x} ".format(byte.uint))
				byte_count += 1
		return byte_count


	# save all trickle configuration data
	def save_trickle(self):
		for segment, data in enumerate(trickle):
			save_hex(data, "segment{}_trickle.txt".format(segment))


	# process and update trickle configuration data
	def compile(self):
		for input_data in self.inputs:
			parser_name = input_data["parser"]
			file_name = path.join(self.base_path, input_data["file"])
			parser = getattr(self.parsers, parser_name)()
			register_data = parser.parse(file_name)
			if "config" in input_data:
				self.encoder = Encoder(input_data["config"])
			else:
				self.encoder = Encoder({})
				input_data["config"] = {}
			self.process_commands_before(input_data)
			self.process_commands_each(register_data, input_data)

		for output_data in self.outputs:
			segment = output_data["segment"]
			file_name = path.join(self.base_path, output_data["file"])
			data = self.trickle[segment]
			output_data["length"] = self.save_hex(data, file_name)


