#!/usr/bin/env python3

from hcc_star.driver_amac import DriverAMAC as DriverAMAC

import argparse
from time import sleep


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


def main():
  parser = argparse.ArgumentParser(description="Utility for sending basic AMAC commands")
  parser.add_argument('command', choices=["read", "next", "write", "setid", "set_id", "configure"], help=('AMAC command to run'), type=str)
  parser.add_argument('-fd', '--fdaq_duration', help='For how many seconds fdaq command should record data (default 3)', default=3, required=False, type=int)
  parser.add_argument('-sd', '--send_delay', help='For how many seconds to wait after issuing fdaq command before sending register read or L0A command (default 1.0)', default=1.0, required=False, type=float)
  parser.add_argument('-pi', '--pads_id', help='Wirebond ID of the chip (default 0x1f)', default="0x1f", required=False, type=str)
  parser.add_argument('-ei', '--efuse_id', help='Efuse ID (default broadcast)', default="0b11111111111111111111", required=False, type=str)
  parser.add_argument('-ai', '--amac_id', help='AMAC ID', required=True, type=str)
  parser.add_argument('-ri', '--register_id', help='Register address (default 0)', default="0", required=False, type=str)
  parser.add_argument('-rd', '--register_data', help='Data to write to the register (default 0)', default="0", required=False, type=str)
  parser.add_argument('-d', '--felix_device', help='FELIX device ID (default 0)', default="0", required=False, type=str)
  parser.add_argument('-e_in', '--elink_in', help='ToHost elink ID where the AMAC data is received from (default 03f)', default="03f", required=False, type=str)
  parser.add_argument('-e_out', '--elink_out', help='FromHost elink ID connected the AMAC input (default 03f)', default="03f", required=False, type=str)
  parser.add_argument('-p', '--path_to_ftools', help='Specify path to the ftools (fupload, fdaq, fcheck)', default="", required=False, type=str)
  parser.add_argument('-v', '--verbose', help='Verbose output (-vv = even more verbose)', action="count", default=0)
  parser.add_argument('-dr', '--dry_run', help='Dry run: no actual commands or data are sent', action="store_true")
  parser.add_argument('-l', '--listen', help='Capture AMAC response when sending write and set_id commands', action="store_true")
  parser.add_argument('-R', '--reset', help='Pass "-R" flag when calling fdaq/fdaqm', action="store_true")
  parser.add_argument('-bg', '--bg', help='BG setting for the AMAC configuration (default 0x00008e8c)', default="0x00008e8c", required=False, type=str)
  parser.add_argument('-de', '--descriptor', help='FELIX ToHost descriptor number', default=0, required=False, type=int)

  global args
  args = parser.parse_args()

  driver = DriverAMAC(dry_run=args.dry_run, 
    elink_in=args.elink_in, elink_out=args.elink_out, felix_device=args.felix_device,
    path_to_ftools=args.path_to_ftools, raise_on_error=True,
    verbose=args.verbose, fdaq_duration=args.fdaq_duration,
    delay_before_send=args.send_delay, reset=True)

  if (args.command == 'read'):
    driver.read(args.amac_id, args.register_id)
    print(driver.response)

  elif (args.command == 'write'):
    driver.write(args.amac_id, args.register_id, args.register_data, listen=args.listen) 

  elif (args.command == 'next'):
    driver.read_next(args.amac_id)
    print(driver.response)

  elif (args.command == 'setid' or args.command == 'set_id'):
    driver.set_id(args.amac_id, args.pads_id, args.efuse_id, listen=args.listen)  

  # write default configuration to AMAC
  elif (args.command == 'configure'):
    driver.configure(args.amac_id, args.bg)

  else:
    parser.error("Unsupported command: {}".format(args.command))
    
  return 0


# if __name__ == '__main__':
#   try:
#     main()
#   except NameError as e:
#     raise(e)
#   except TypeError as e:
#     raise(e)
#   except Exception as e:
#     print(e)

if __name__ == '__main__':
  main()