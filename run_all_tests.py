#!/usr/bin/env python3

from hcc_star.parser import Parser as HCCParser
from hcc_star.parser import TYP
from hcc_star.driver import get_driver
from hcc_star.parser import get_target_type

import argparse
import sys
import time
from time import sleep
from termcolor import colored


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"

report_prefix = "    "
err_prefix = "!!! "


# Implement a basic test engine and runs a set of pre-defined tests
# Sets shell return code:
# - 0 if all test passed
# - 1 if at least one test fails


# TODO:
# - Integrate desctiptions into the tests
# - put tests and test engine into a class, make tests iterable 

def main():
  parser = argparse.ArgumentParser(description="Utility for running HCC* basic datapath tests")
  parser.add_argument('-d', '--felix_device', help='FELIX board ID (default 0)', default="0", required=False, type=str)
  parser.add_argument('-e_dout', '--elink_dout', help='ToHostelink ID where the Strips data is received from (default 00)', default="00", required=False, type=str)
  parser.add_argument('-e_lcb', '--elink_LCB', help='FromHost elink ID connected to LCB (default 00)', default="00", required=False, type=str)
  parser.add_argument('-e_r3l1', '--elink_R3_L1', help='FromHost elink ID connected to R3_L1 (default 00)', default="00", required=False, type=str)
  parser.add_argument('-p', '--path_to_ftools', help='Specify path to the ftools (fupload, fdaq, fcheck)', default="", required=False, type=str)
  parser.add_argument('-v', '--verbose', help='Verbose output (-vv = even more verbose)', action="count", default=0)
  parser.add_argument('-k', '--keep_going', help='Keep going even if one test fails', action="store_true")
  parser.add_argument('-r', '--reset', help='Issue soft reset before each data acquisition', action="store_true")
  parser.add_argument('-pe', '--ignore_parse_errors', help='Keep going even if parse errors are encountered; invoke robust search for the register read packets', action="store_true")
  parser.add_argument('-s', '--single', help='Run a single test given name', default="", required=False, type=str)
  parser.add_argument('-hi', '--hcc_id', help='HCC ID (default broadcast)', default="0b1111", required=False, type=str)
  parser.add_argument('-ai', '--abc_id', help='ABC ID (default broadcast)', default="0b1111", required=False, type=str)
  parser.add_argument('-drv', '--driver', help='ITk Strips driver version (software, firmware), default software', default="software", required=False, type=str)
  parser.add_argument('-nfh', '--nibble_order_from_host', help='Nibble order from host (inverse or direct, default direct)', default="direct", required=False, type=str)
  parser.add_argument('-nth', '--nibble_order_to_host', help='Nibble order to host (inverse or direct, default direct)', default="direct", required=False, type=str)
  parser.add_argument('-fd', '--fdaq_duration', help='For how many seconds fdaq command should record data (default 3)', default=3, required=False, type=int)
  parser.add_argument('-sd', '--send_delay', help='For how many seconds to wait after issuing fdaq command before sending (register read) command (default 1.0)', default=1.0, required=False, type=float)
  parser.add_argument('-se', '--serial', help='ASIC serial for dynamic addressing (defalt 0)', default="0", required=False, type=str)
  parser.add_argument('-dr', '--dry_run', help='Dry run: no actual commands or data are sent', action="store_true")
  parser.add_argument('-q', '--quick', help='Quick test: use a reduced test set', action="store_true")


  global args
  args = parser.parse_args()

  driver = get_driver(args.driver)(dry_run=args.dry_run, target="HCC",
    elink_LCB=args.elink_LCB, elink_R3_L1=args.elink_R3_L1, elink_dout=args.elink_dout,
    felix_device=args.felix_device, path_to_ftools=args.path_to_ftools,
    verbose=min(args.verbose - 1, 0), raise_on_error=True, hcc_id=args.hcc_id, abc_id=args.abc_id,
    fdaq_duration=args.fdaq_duration, delay_before_send=args.send_delay, 
    nibble_order_to_host=args.nibble_order_to_host,
    nibble_order_from_host=args.nibble_order_from_host,
    reset=args.reset)

  tests = [test_FromHost_elink_send_HCC_reset,
          test_timeout_enabled,
          test_hcc_receive_hpr,
          test_hcc_register_write_disable_hpr,
          test_hcc_register_read, 
          test_hcc_register_read_write,
          test_hcc_decode_errors,
          test_abc_star_enable,
          test_abc_register_write_disable_hpr,
          test_abc_register_read]

  tests_advanced = [test_trickle_reg_read,
                    test_trickle_retrigger,
                    test_trickle_overwrite]

  tests_quick = [
    test_FromHost_elink_send_HCC_reset,
    test_timeout_enabled,
    test_hcc_register_write_disable_hpr,
    test_hcc_register_read_write,
    test_abc_star_enable,
    test_abc_register_write_disable_hpr,
    test_abc_register_read
  ]


  if args.driver == "firmware":
    # tests = tests + tests_advanced
    print("Trickle configuration tests are broken for this tool version, skipping...")

  if args.quick:
    tests = tests_quick

  if args.single:
    try:
      tests = [globals()[args.single]]
    except KeyError as e:
      print("{} test is not defined".format(e))
      return -1


  total = len(tests)
  passed = 0
  time_start = time.time()  

  for test in tests:
    try:
      print_test_name(test.__name__)
      test(driver)
      print_test_pass()
      passed += 1
      sleep(1)
    except KeyError as e:
      raise e
    except NameError as e:
      raise e
    except AttributeError as e:
      raise e
    except TypeError as e:
      raise e
    except KeyboardInterrupt as e:
      print("\nCtrl+C")
      break
    except Exception as e:
      print_test_fail()
      print_error_message(e)
      if not args.keep_going:
        break
  time_elapsed = round(time.time() - time_start)
  print("-------------------------")
  print("Passed {}/{} tests in {} seconds".format(passed, total, time_elapsed))
  return (total != passed)


def print_test_name(name):
  if args.verbose > 0:
    print("Running: " + name + ":")
  else:
    print("{:<50}".format("Running: " + name), end='', flush=True)


def report(msg, verbosity = 1):
  if args.verbose >= verbosity:
    print(report_prefix, end='')
    print(msg)


def print_test_pass():
  print("[" + colored("PASS", 'green') + "]")
  if args.verbose > 0:
    print("\n")


def print_test_fail():
  print("[" + colored("FAIL", 'red') + "]")


def print_error_message(e):
  print(err_prefix + "{}".format(e))
  if args.verbose > 0:
    print("\n")

# =========== Tests and helpers are defined below =============================

# Tries to find the register value by scanning the output file
def read_register_try_harder(driver, reg_addr, target=None):
  report(("Scanning HCC*/ABC* data for a specific packet with "
    + "register_address={} (trying harder)").format(reg_addr))
  data = driver.find_register_read(reg_addr, target=target)
  if not data:
    raise Exception("Can't find register read packets for register_address={}".format(reg_addr))      
  return data

# Reads a register and returns the value (if everything goes well)
def read_register_wrapper(driver, reg_addr, target="HCC", ignore_parse_errors=None,
    do_send_command=True):
  if do_send_command:
    report("Send register read command: {}*, register {}".format(target, reg_addr))
    driver.read_register(reg_addr, target)

  if driver.dry_run: return
    
  errors = driver.hcc_parser.parse_errors
  timeout = driver.hcc_parser.parse_timeout
  max_parse_time = driver.hcc_parser.max_parse_time  
  ignore_parse_errors = ignore_parse_errors or args.ignore_parse_errors

  errors_msg = (("Encountered {} parse errors in HCC* data. "
    + "Please run \"./parse_file.py dataout.dat | less\" to "
    + "inspect the data; ").format(errors))

  if driver.fcheck_error:
    errors_msg += ("; FELIX data contains error blocks!; "
      + "Run \"fcheck fdaq_data-1.dat\" to inspect the blocks.")

  if errors != 0: # check for parse errors
    if ignore_parse_errors:
      report(errors_msg)
    else:
      raise Exception(errors_msg)

  if timeout: # if parser timed out
    report(("HCC*/ABC* response parser timed out ({}s parsing "
    + "time allowed)".format(max_parse_time)))

  data = driver.hcc_parser.packet_map[get_target_type(target)]
  
  if not data: # no register read packets were received
    msg = "Could not find any read packets from {}*".format(target)
    if (errors != 0 or timeout) and ignore_parse_errors:
      report(msg)
      data = read_register_try_harder(driver, reg_addr, target=target)
    else:
      raise Exception(msg)

  # find packet with the correct register ID (if it exists)
  packet_id = -1
  for i in range(len(data)):
    if data[i]['reg_addr'].int == reg_addr:
      packet_id = i
      break

  actual_reg_addr = data[packet_id]['reg_addr'].int
  if reg_addr != actual_reg_addr:
    # register read packet was found, but the address is wrong
    msg = "Incorrect register address, expected {}, was {}".format(reg_addr, actual_reg_addr)
    if (errors != 0 or timeout) and ignore_parse_errors:
      report(msg)
      data = read_register_try_harder(driver, reg_addr, target=target)
    else:
      raise Exception(msg)

  register_data = data[packet_id]['reg_data'].int
  report("Response: register {}, value=0x{:04X}".format(actual_reg_addr, register_data))
  return data[packet_id]


# Reads the register (if enabled) and checks that the returned value equals to the expected value
def test_register_read_helper(driver, expected_reg_addr, expected_reg_data,
  target="HCC", do_send_command=True):
  if do_send_command:
    report("Reading {}* register {}, expected value = 0x{:04X}".format(
      target, expected_reg_addr, expected_reg_data))
  data = read_register_wrapper(driver, expected_reg_addr, target=target,
    do_send_command=do_send_command)
  if driver.dry_run: return

  reg_addr = data['reg_addr'].int
  reg_data = data['reg_data'].int
  if reg_addr != expected_reg_addr:
    raise Exception("Incorrect register address, expected {}, was {}".format(
      expected_reg_addr, reg_addr))
  if reg_data != expected_reg_data:
    raise Exception("Incorrect register {} data, expected 0x{:04X}, was 0x{:04X}".format(
      reg_addr, expected_reg_data, reg_data))
  report("Correct response is received")
  if do_send_command:
    sleep(1)


def read_hcc_LCB_error_counter(driver):
  report("Reading HCC* LCB error count")
  lcb_error_counter = read_register_wrapper(driver, 4, "HCC")['reg_data'][0:16].int
  report("LCB error count = {}".format(lcb_error_counter))
  return lcb_error_counter

# --------------------- Actual tests begin here -------------------------------

def test_hcc_decode_errors(driver):
  lcb_error_counter = read_hcc_LCB_error_counter(driver)
  if lcb_error_counter != 0:
    report("Clear HCC* LCB error counter (write 0b1000 to register HCC* 16)")
    driver.write_register(16, 0b1000)
    lcb_error_counter = read_hcc_LCB_error_counter(driver)
    if lcb_error_counter != 0:
      raise Exception("Cannot clear HCC* LCB error counter (lcb_errcount={})".format(lcb_error_counter))
  report("LCB error counter reads 0x0, proceed with sending commands")
  report("Send some idle frames")
  sleep(1)
  lcb_error_counter = read_hcc_LCB_error_counter(driver)
  if lcb_error_counter != 0:
    raise Exception("LCB error counter changed after sending idle frames (lcb_errcount={})".format(lcb_error_counter))
  report("Send some fast commands")
  # these fast commands should not meaningfully affect the ASIC state
  driver.fast_command(3) # ABC register reset
  driver.fast_command(4) # ABC SEU reset
  driver.fast_command(7) # ABC hit counter reset
  driver.fast_command(10) # ABC slow command reset
  lcb_error_counter = read_hcc_LCB_error_counter(driver)
  if lcb_error_counter != 0:
    raise Exception("LCB error counter changed after sending fast commands (lcb_errcount={})".format(lcb_error_counter))
  report("Send some L0A commands")
  driver.L0A(False, 0b0101, 0b101010)
  driver.L0A(True,  0b0000, 0b111011)
  driver.L0A(False, 0b0001, 0b000001)
  driver.L0A(True,  0b1111, 0b111111)
  lcb_error_counter = read_hcc_LCB_error_counter(driver)
  if lcb_error_counter != 0:
    raise Exception("LCB error counter changed after sending L0A commands (lcb_errcount={})".format(lcb_error_counter))
  report("Send some register reads (might take a while)")
  driver.read_register(6, target="HCC")
  driver.read_register(32, target="HCC")
  driver.read_register(42, target="HCC")
  driver.read_register(1, target="HCC")
  lcb_error_counter = read_hcc_LCB_error_counter(driver)
  if lcb_error_counter != 0:
    raise Exception("LCB error counter changed after sending register read commands (lcb_errcount={})".format(lcb_error_counter))
  report("Send some register writes")
  driver.write_register(33, 0b101010101)
  driver.write_register(33, 0b000010000)
  driver.write_register(33, 0xFFFF)
  driver.write_register(33, 0)
  driver.write_register(34, 0b101010101)
  driver.write_register(34, 0b000010000)
  driver.write_register(34, 0xFFFF)
  driver.write_register(34, 0)
  lcb_error_counter = read_hcc_LCB_error_counter(driver)
  if lcb_error_counter != 0:
    raise Exception("LCB error counter changed after sending register write commands (lcb_errcount={})".format(lcb_error_counter))


# Send a reset, should pass as long as the elink and ftools are working
def test_FromHost_elink_send_HCC_reset(driver):
  report("Sending reset command to elink {}".format(args.elink_LCB))
  driver.full_reset()
  report("Data was sent to elink {} successfully".format(args.elink_LCB))  
  if args.serial != "0":
    report("Set HCC* ID={} for ASIC with serial={}".format(args.hcc_id, args.serial))


def test_timeout_enabled(driver):
  report("Check if FELIX timeout is enabled")
  timeout = int(driver.felix_config_get("TIMEOUT_CTRL_ENABLE"), 0)
  if timeout == 0:
    raise Exception("FELIX has timeout disabled (TIMEOUT_CTRL_ENABLE=0x0); this will cause issues HCC* when HPR packets are disabled")
  report("Response: TIMEOUT_CTRL_ENABLE=0x{:X}".format(timeout))


# See if HCC* HPR packets arrive and LCB_locked = True
def test_hcc_receive_hpr(driver):
  report("Flushing out broken data after reset")
  read_register_wrapper(driver, 42, ignore_parse_errors=True)
  report("Read out register 42 (arbitrary), look for HPR packets from HCC*")
  read_register_wrapper(driver, 42)
  data = driver.hcc_parser.packet_map[TYP.HCC_high_priority]
  if not data:
    raise Exception("Did not receive HPR packets from HCC*")
  register_data = data[-1]['reg_data']
  report("Found valid HCC* HPR register packets, register_data={}".format(register_data))  
  register_data.reverse()
  lcb_locked = register_data[1]
  if not lcb_locked:
    raise Exception("HPR packets from HCC* show LCB_locked = False")
  report("Verified that HPR packets from HCC* show LCB_locked = True")


# See if HCC* registers can be read
def test_hcc_register_read(driver):
  test_register_read_helper(driver, 42, 0x20001)
  test_register_read_helper(driver, 35, 0xFF3B05)
  test_register_read_helper(driver, 44, 0x18E)
  test_register_read_helper(driver, 48, 0x406600)


# disable HCC* HPR registers, see if they stop coming
def test_hcc_register_write_disable_hpr(driver):
  # first enable HCC* HPR packets
  report("Reset HCC* registers")
  driver.hcc_register_reset()  
  sleep(1)
  # verify HCC* HPR packets are coming
  report("Flushing out broken data after reset")
  read_register_wrapper(driver, 42, ignore_parse_errors=True)
  report("Read out register 42 (arbitrary), look for HPR packets from HCC*")
  read_register_wrapper(driver, 42) # listen() will fail if no data now arrives
  data = driver.hcc_parser.packet_map[TYP.HCC_high_priority]
  if not data:
    raise Exception("Cannot receive HPR packets from HCC* before disabling them")
  register_data = data[-1]['reg_data']
  report("Found valid HCC* HPR register packets, register_data={}".format(register_data))  
  report("Disable HCC* HPR packets (write 0x1 to register 16)")
  driver.write_register(16, 1)
  sleep(1)  
  # the double-read command circumvents the bug where the ASIC sends broken data
  # upon receiving "disable HPR" command and flushes remaining HPR packets out
  report("Flushing out broken data after disabling HPR packets")
  read_register_wrapper(driver, 42, ignore_parse_errors=True) # listen() will fail because no data now arrives
  report("Read out register 42 (arbitrary), look for HPR packets from HCC*")
  read_register_wrapper(driver, 42) 
  data = driver.hcc_parser.packet_map[TYP.HCC_high_priority]
  if data:
    raise Exception("Could not disable HPR packets from HCC*")
  report("Verified that the HCC* HPR packets are no longer received")


def test_register_read_write_helper(driver, reg_addr, reg_data, reg_data_old, read_before_write):
  if read_before_write:
    report("Verify the old value of register {} before writing".format(reg_addr))
    test_register_read_helper(driver, reg_addr, reg_data_old)
  report("Write 0x{:04X} to register {}".format(reg_data, reg_addr))
  driver.write_register(reg_addr, reg_data)
  report("Read back register {}".format(reg_addr))
  test_register_read_helper(driver, reg_addr, reg_data)


# See if HCC* registers can be written
def test_hcc_register_read_write(driver):
  test_register_read_write_helper(driver, 40, 0x7ff, 0, True)
  test_register_read_write_helper(driver, 40, 0b10010, 0, False)
  test_register_read_write_helper(driver, 40, 0x1, 0, False)
  test_register_read_write_helper(driver, 40, 0b1010101, 0, False)
  test_register_read_write_helper(driver, 40, 0, 0, False)


# See if ABC* can be enabled (ABC* HPR packets are received, LCB_locked = True)
def test_abc_star_enable(driver):
  report("Enabling ABC* chip(s)")
  driver.enable_ABC_star()
  sleep(1)
  report("Read out register 42 (arbitrary), look for HPR packets from ABC*")
  read_register_wrapper(driver, 42) # listen() will fail if no data now arrives
  data = driver.hcc_parser.packet_map[TYP.ABC_high_priority]
  if not data:
    raise Exception("Did not receive register read packets from ABC*")
  register_data = data[-1]['reg_data']
  report("Found valid ABC* HPR register packets, register_data={}".format(register_data))  
  register_data.reverse()  
  lcb_locked = register_data[12]
  if not lcb_locked:
    raise Exception("HPR packets from ABC* show LCB_locked = False")
  report("Verified that HPR packets from ABC* show LCB_locked = True")


# disable ABC* HPR registers, see if they stop coming
def test_abc_register_write_disable_hpr(driver):
  # first enable ABC* HPR packets
  report("Reset ABC* registers to enable ABC* HPR packets (if not enabled already)")
  driver.abc_register_reset()
  sleep(1)
  # verify ABC* HPR packets are coming
  report("Read out register 42 (arbitrary), look for HPR packets from ABC*")
  read_register_wrapper(driver, 42) # listen() will fail if no data now arrives
  data = driver.hcc_parser.packet_map[TYP.ABC_high_priority]
  if not data:
    raise Exception("Cannot receive HPR packets from ABC* before disabling them")
  register_data = data[-1]['reg_data']
  report("Found valid ABC* HPR register packets, register_data={}".format(register_data))  
  # disable ABC* HPR packets
  driver.write_register(0, 0b100, target="ABC")
  sleep(1)
  # the double-read command circumvents the bug where the ASIC sends broken data
  # upon receiving "disable HPR" command and flushes remaining HPR packets out
  report("Flushing out broken data after disabling HPR packets")
  read_register_wrapper(driver, 42, ignore_parse_errors=True) # listen() will fail because no data now arrives
  report("Read out register 42 (arbitrary), look for HPR packets from ABC*")
  read_register_wrapper(driver, 42) 
  data = driver.hcc_parser.packet_map[TYP.ABC_high_priority]
  if data:
    raise Exception("Could not disable HPR packets from ABC*")
  report("Verified that the ABC* HPR packets are no longer received")


# Try reading an ABC* register
def test_abc_register_read(driver):
  # enable ABC* register read function
  report("Enabling ABC* register read function (write 0x400 to ABC* register 0x20)")
  driver.write_register(0x20, 0x400, target="ABC")
  test_register_read_helper(driver, 0x20, 0x400, target="ABC")
  test_register_read_helper(driver, 0x26, 0xffff, target="ABC")


def add_reg_read_to_trickle_memory(driver, target, reg_addr, idle_count=0):
  report(("Save register read command to trickle configuration memory: {}*, register {}"
    ).format(target, reg_addr))
  driver.read_register(reg_addr, target="HCC", destination="trickle")
  if idle_count > 0:
    for _ in range(idle_count):
      driver.send_idle(destination="trickle")


def test_trickle_reg_read(driver):
  add_reg_read_to_trickle_memory(driver, "HCC", reg_addr=37, idle_count=10)
  add_reg_read_to_trickle_memory(driver, "HCC", reg_addr=48, idle_count=10)
  report("Writing trickle configuration memory")
  driver.write_trickle_memory()
  sleep(1)
  report("Issuing a trickle trigger pulse, recording the response")
  driver.issue_trickle_trigger_pulse()
  report(driver.hcc_parser.packet_map[get_target_type("HCC")], verbosity=2)
  test_register_read_helper(driver, expected_reg_addr=37, expected_reg_data=0x4,
     target="HCC", do_send_command=False)
  test_register_read_helper(driver, expected_reg_addr=48, expected_reg_data=0x406600,
     target="HCC", do_send_command=False)

def test_trickle_retrigger(driver):
  report("Issuing a trickle trigger pulse, recording the response")
  driver.issue_trickle_trigger_pulse()
  report(driver.hcc_parser.packet_map[get_target_type("HCC")], verbosity=2)
  test_register_read_helper(driver, expected_reg_addr=37, expected_reg_data=0x4,
     target="HCC", do_send_command=False)
  test_register_read_helper(driver, expected_reg_addr=48, expected_reg_data=0x406600,
     target="HCC", do_send_command=False)

def test_trickle_overwrite(driver):
  add_reg_read_to_trickle_memory(driver, "HCC", reg_addr=41, idle_count=0)
  add_reg_read_to_trickle_memory(driver, "HCC", reg_addr=35, idle_count=0)
  report("Writing trickle configuration memory")
  driver.write_trickle_memory()
  sleep(1)
  report("Issuing a trickle trigger pulse, recording the response")
  driver.issue_trickle_trigger_pulse()
  report(driver.hcc_parser.packet_map[get_target_type("HCC")], verbosity=2)
  test_register_read_helper(driver, expected_reg_addr=41, expected_reg_data=0x20001,
     target="HCC", do_send_command=False)
  test_register_read_helper(driver, expected_reg_addr=35, expected_reg_data=0xFF3B05,
     target="HCC", do_send_command=False)

if __name__ == '__main__':
  sys.exit(main())
