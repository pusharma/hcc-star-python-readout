#Register  0:       SEU1 00000000
#Register  1:       SEU2 00000000
#Register  2:       SEU3 00000000
#Register  3:   FrameRaw 74b78557
#Register  4:     LCBerr 00000000
#Register  5:  ADCStatus 00000187
#Register  6:     Status 00000003
#Register 15:        HPR 74b70033
#Register 17: Addressing 00004204
Register 32:     Delay1 02200000
Register 33:     Delay2 44444444
Register 34:     Delay3 00000444
Register 35:       PLL1 00ff3b05
Register 36:       PLL2 00000000
Register 37:       PLL3 00000004
Register 38:       DRV1 0fffffff
Register 39:       DRV2 00000014
Register 40:   ICenable 000003ff
Register 41:     OPmode 00020001
Register 42:     OPmode 00020001
Register 43:       Cfg1 00000000
Register 44:       Cfg2 0000018e
Register 45:     ExtRst 00710003
Register 46:    ExtRstC 00710003
Register 47:     ErrCfg 00000000
Register 48:     ADCcfg 00416601