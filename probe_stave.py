#!/usr/bin/env python3

from hcc_star.parser import Parser as HCCParser
from hcc_star.parser import TYP
from hcc_star.driver import get_driver
from hcc_star.parser import get_target_type

import argparse
import subprocess
import csv
import os, pdb

from operator import itemgetter
from termcolor import colored, cprint
from tabulate import tabulate


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"

communication_register = 17

lpgbt_map_phase1 = {
  0 : {
    "output_elinks" : "018,01a,01c,01e",
    "input_elinks" : "001,005,009,00d,011,015,019,01d,041,045,049,04d,051,055,059,05d"
  },
  1 : {
    "output_elinks" : "080,086,08a,08e",
    "input_elinks" : "081,085,089,08d,091,095,099,09d,0c1,0c5,0c9,0cd,0d1,0d5,0d9,0dd"
  }
}

lpgbt_map_phase2 = {
  0 : {
    "output_elinks" : "001,006,00b,010",
    "input_elinks" : "000,002,004,006,008,00a,00c,00e,010,012,014,016,018,01a"
  },
  1 : {
    #"output_elinks" : "041,046,04b,050",
    # LCB and R3L1 elinks are swapped on slave side
    "output_elinks" : "041,049,04e,053",
    "input_elinks" : "040,042,044,046,048,04a,04c,04e,050,052,054,056,058,05a"
  }
}

lpgbt_phase2_offsets = {
  "output_elinks" : [ 0x1,0x6,0xb,0x10 ],
  "output_elinks_weird_stave" : [ 0x1,0x9,0xe,0x13 ],
  "output_elinks_r3l1" : [ 0x4,0x9,0xe,0x13 ],
  "input_elinks" : [0x0,0x2,0x4,0x6,0x8,0xa,0xc,0xe,0x10,0x12,0x14,0x16,0x18,0x1a]
}


def main():
  parser = argparse.ArgumentParser(description="Utility for finding which elinks each HCC* chip is connected to on a Star Chip Stave")
  parser.add_argument('-fh', "--from_host", help='List of FromHost elinks to scan (hex, comma separated)',
    default="018,01a,01c,01e") # where the bypass elinks are at for lpGBT 0
  parser.add_argument('-th', "--to_host", help='List of ToHost elinks to scan (hex, comma separated)',
    default="001,005,009,00d,011,015,019,01d,041,045,049,04d,051,055,059,05d") # where HCC* responded in tests last year (lpGBT 0)
  parser.add_argument('-l', '--lpgbt', help='lpGBT link ID (default 0)', required=False, type=str)
  parser.add_argument('-t', "--test_communication", action="store_true", help="Test basic communications of each found HCC* chip")
  parser.add_argument('-fd', '--fdaq_duration', help='For how many seconds fdaq command should record data (default 3)', default=3, required=False, type=int)
  parser.add_argument('-sd', '--send_delay', help='For how many seconds to wait after issuing fdaq command before sending register read or L0A command (default 1.0)', default=1.0, required=False, type=float)
  parser.add_argument('-d', '--felix_device', help='FELIX device ID (default 0)', default="0", required=False, type=str)
  parser.add_argument('-p', '--path_to_ftools', help='Specify path to the ftools (fupload, fdaq, fcheck)', default="", required=False, type=str)
  parser.add_argument('-v', '--verbose', help='Verbose output (-vv = even more verbose)', action="count", default=0)
  parser.add_argument('-nfh', '--nibble_order_from_host', help='Nibble order from host (inverse or direct, default direct)', default="direct", required=False, type=str)
  parser.add_argument('-nth', '--nibble_order_to_host', help='Nibble order to host (inverse or direct, default direct)', default="direct", required=False, type=str)
  parser.add_argument('-dr', '--dry_run', help='Dry run: no actual commands or data are sent', action="store_true")
  parser.add_argument('-drv', '--driver', help='ITk Strips driver version (software, firmware, trickle), default software', default="software", required=False, type=str)
  parser.add_argument('-R', '--reset', help='Pass "-R" flag when calling fdaq/fdaqm', action="store_true")
  parser.add_argument('-ipp', '--ignore_physics_packets', help='Parser skips physics packets (use if the data coming from HCC*/ABC* is heavily corrupt)', action="store_true")
  parser.add_argument('-de', '--descriptor', help='FELIX ToHost descriptor number', default=0, required=False, type=int)
  parser.add_argument('-out', '--output', help='Path to directory to save reports', default="./data/reports", required=False, type=str)
  parser.add_argument('-ws', '--weird_stave', help='This specific stave is wired incorrectly: R3L1 singlals on segments 1-3 are wired to LCB lines and vice versa (only supports -drv software)', action="store_true")
  parser.add_argument('-r3', '--r3l1', help='Use R3L1 links to send out LCB commands, rather than LCB links (only supports -drv software)', action="store_true")

  global args
  args = parser.parse_args()

  if args.r3l1 and args.driver != "software":
    parser.error("Flag --r3l1 can only be used with --driver software")

  if args.weird_stave and args.driver != "software":
    parser.error("Flag --weird_stave can only be used with --driver software")

  if args.r3l1 and args.weird_stave:
    parser.error("Flags --weird_stave and --r3l1 are incompatible, please select only one!")

  driver = get_driver(args.driver)(dry_run=args.dry_run,
    path_to_ftools=args.path_to_ftools,
    felix_device=args.felix_device,
    verbose=(args.verbose), bc=0, fdaq_duration=args.fdaq_duration,
    nibble_order_to_host=args.nibble_order_to_host,
    nibble_order_from_host=args.nibble_order_from_host,
    raise_on_error=False,
    delay_before_send=args.send_delay, reset=args.reset, 
    ignore_physics_packets=args.ignore_physics_packets,
    descriptor=args.descriptor)

  strips_map = {}

  if args.lpgbt is not None:
    regmap_version = driver.regmap_major
    if regmap_version >= 5:
      # this is regmap version >= rm-5.0
      lpgbt_id = int(args.lpgbt, 0)
      input_elinks_str = compute_lpgbt_elinks(lpgbt_id, lpgbt_phase2_offsets["input_elinks"])
      if args.weird_stave:
        output_elinks_str = compute_lpgbt_elinks(lpgbt_id, lpgbt_phase2_offsets["output_elinks_weird_stave"])
      elif args.r3l1:
        output_elinks_str = compute_lpgbt_elinks(lpgbt_id, lpgbt_phase2_offsets["output_elinks_r3l1"])
      else:
        output_elinks_str = compute_lpgbt_elinks(lpgbt_id, lpgbt_phase2_offsets["output_elinks"])

      if args.verbose:
        print(f"Probing FromHost elinks: {output_elinks_str}")
        print(f"Probing ToHost elinks: {input_elinks_str}")

    elif regmap_version >= 4:
      # this is regmap version >= rm-4.0
      input_elinks_str = lpgbt_map_phase1[int(args.lpgbt, 0)]["input_elinks"]
      output_elinks_str = lpgbt_map_phase1[int(args.lpgbt, 0)]["output_elinks"]
    else:
      print("Unsupported REG_MAP_VERSION={}".format(regmap_version))
      return 38
  else:
    input_elinks_str = args.to_host
    output_elinks_str = args.from_host
  
  input_elinks = input_elinks_str.split(',')
  output_elinks = output_elinks_str.split(',')

  if not os.path.exists(args.output):
    os.makedirs(args.output)

  for output_elink in output_elinks:
    print("Probing all HCC* chips listening to elink {}...".format(output_elink))
    driver.elink_out = output_elink
    driver.read_register(communication_register, target="HCC",
      hcc_id="0b1111", abc_id="0b1111")

    if driver.block_count == 0:
      print("No data was received from FELIX device {}".format(args.felix_device))
      continue

    #input("Press Enter to continue...")
    for input_elink in input_elinks:
      print("Checking for responses on ToHost elink {}".format(input_elink))      
      driver.elink_in = input_elink
      driver.issue_fcheck()
      driver.parse_response()      
      try:        
        data = read_register_wrapper(driver, communication_register)
        comm_id = data['reg_data'][0:4].uint
        efuse_id = data['reg_data'][8:32].uint
        elink_tx = int(output_elink, 16)
        elink_rx = int(input_elink, 16)
        cprint("Response: {:08x}; HCC* with eFuseID={:06x}, HCC_ID={:x} responded at elinks tx={}, rx={}".format(
          data['reg_data'].uint, efuse_id, comm_id, elink_tx, elink_rx), 'green')
        strips_map["{}-{}-{}".format(data['reg_data'].uint, elink_tx, elink_rx)] = {
          "efuse_id" : efuse_id, 
          "elink_tx" : elink_tx, 
          "elink_rx" : elink_rx,
          "comm_id" : comm_id,
          "test_result" : None}
      except:
        pass

  # run detailed test for each HCC* if needed

  if args.test_communication:
    test_communication(strips_map)

  # report the test results
  if len(strips_map) == 0:
    cprint("\nNo HCC* chips was found", 'red')
    table = None
  else:
    print("\n")
    table = make_table(strips_map)
    print(tabulate(table, headers="firstrow"))
    print("\nTotal: {} HCC* chips".format(len(strips_map)))

  if not table is None:
    export_csv_report(table, "lpgbt{}_probe_results.csv".format(args.lpgbt))
    
# ---------------------------------------------------------------------------------

def compute_lpgbt_elinks(lpgbt, links):
  links_shifted = [link + 0x40 * lpgbt for link in links]
  links_formatted = [f"{link:03x}" for link in links_shifted]
  return ",".join(links_formatted)


def test_communication(strips_map):
  for (hcc_id, link) in strips_map.items():
    print("\n========= Testing communication with HCC_ID={} =========".format(hcc_id))

    dry_run = "-dr" if args.dry_run else ""
    verbosity = ""
    if args.verbose > 1:
      verbosity = "-vv"
    elif args.verbose == 1:
      verbosity = "-v"
    path_to_ftools = "" if args.path_to_ftools == "" else "-p={}".format(args.path_to_ftools)
    cmd = ("./run_all_tests.py -q {} {} {} -k -pe" +
      " -e_dout={:03x} -e_lcb={:03x} -hi={} -drv={} -nfh={}, -nth={}" + 
      " -fd={} -sd={}").format(
      dry_run, verbosity, path_to_ftools, link['elink_rx'], link['elink_tx'],
      hcc_id, args.driver, args.nibble_order_from_host, args.nibble_order_to_host,
      args.fdaq_duration, args.send_delay)

    report(cmd)
    test = subprocess.run(cmd, shell=True)
    strips_map[hcc_id]['test_result'] = test.returncode 
      
def make_table(strips_map):
  table = []
  header = ["HCC ID","eFuseID", "TX", "RX"]

  if args.test_communication:
    header.append("Tests")

  table.append(header)

  for (key, data) in strips_map.items():
    line = ["{}".format(data['comm_id']), "{:06x}".format(data['efuse_id']),
      "{}".format(data['elink_tx']), "{}".format(data['elink_rx'])]
    if args.test_communication:
      test_result = data['test_result']
      if test_result is None:
        line.append("N/A")
      elif test_result == 0:
        line.append(colored("PASS", 'green'))
      else:
        line.append(colored("FAIL", 'red'))
    table.append(line)
  return table

# save the report as a csv file
def export_csv_report(table, file_name):
  report_path = os.path.join(args.output, file_name)
  with open(report_path, 'w', newline='') as csvfile:
    report_writer = csv.writer(csvfile)
    for line in table:
      report_writer.writerow(line)

# prints the parsed packet message (or doesn't if not verbose)
def report(msg, verbosity=1):
  colors=['grey', 'white', 'cyan', 'green', 'blue' 'magenta']
  if args.verbose >= verbosity:
    for _ in range(verbosity):
      print("---", end='')
    cprint(msg, colors[verbosity])


# Reads a register and returns the value (if everything goes well)
def read_register_wrapper(driver, reg_addr, target="HCC", ignore_parse_errors=True,
    do_send_command=False):
  if do_send_command:
    report("Send register read command: {}*, register {}".format(target, reg_addr))
    driver.read_register(reg_addr, target)

  if driver.dry_run: return
    
  errors = driver.hcc_parser.parse_errors
  timeout = driver.hcc_parser.parse_timeout
  max_parse_time = driver.hcc_parser.max_parse_time
  ignore_parse_errors = ignore_parse_errors or args.ignore_parse_errors

  errors_msg = (("Encountered {} parse errors in HCC* data. "
    + "Please run \"./parse_file.py dataout.dat | less\" to "
    + "inspect the data; ").format(errors))

  if errors != 0: # check for parse errors
    if ignore_parse_errors:
      report(errors_msg)
    else:
      raise Exception(errors_msg)

  if timeout: # if parser timed out
    report(("HCC*/ABC* response parser timed out ({}s parsing "
    + "time allowed)".format(max_parse_time)))

  data = driver.hcc_parser.packet_map[get_target_type(target)]
  
  if not data: # no register read packets were received
    msg = "Could not find any read packets from {}*".format(target)
    if (errors != 0 or timeout) and ignore_parse_errors:
      report(msg)
      data = read_register_try_harder(driver, reg_addr, target=target)
    else:
      raise Exception(msg)

  # find packet with the correct register ID (if it exists)
  packet_id = -1
  for i in range(len(data)):
    if data[i]['reg_addr'].int == reg_addr:
      packet_id = i
      break

  actual_reg_addr = data[packet_id]['reg_addr'].int
  if reg_addr != actual_reg_addr:
    # register read packet was found, but the address is wrong
    msg = "Incorrect register address, expected {}, was {}".format(reg_addr, actual_reg_addr)
    if (errors != 0 or timeout) and ignore_parse_errors:
      report(msg)
    else:
      raise Exception(msg)

  register_data = data[packet_id]['reg_data'].int
  report("Response: register {}, value=0x{:04X}".format(actual_reg_addr, register_data))
  return data[packet_id]


if __name__ == '__main__':
  main()
