#!/usr/bin/env python3

from tqdm import tqdm
import argparse, os

__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


class PatternGenerator():
    def __init__(self, pattern, elink_width, egroup_width):
        pattern = [int(x, 0) for x in pattern.split(',')]
        for x in pattern:
            if x < 0 or x > 255:
                raise ValueError(f'Non-byte in data pattern: 0x{x:02X}')

        self.pattern = pattern
        self.egroup_width = egroup_width
        self.elink_width = elink_width
        self.pos = 0


    def next_elink_data(self):
        elink_data = 0
        for i in range(0, int(self.elink_width / 8)):
            elink_data = (elink_data << 8) | self.pattern[self.pos]
            if self.pos < len(self.pattern) - 1:
                self.pos += 1
            else:
                self.pos = 0
        return elink_data


    def next(self):
        elink_data = self.next_elink_data()
        egroup_data = 0
        for i in range(0, int(self.egroup_width / self.elink_width)):
            egroup_data = (egroup_data << self.elink_width) | elink_data
        return egroup_data



def main():
    parser = argparse.ArgumentParser(
        description="Utility for configuring FELIX ToHost data emulator memory")
    parser.add_argument('-p', "--pattern", type=str,
        help='Byte pattern to fill the memory with',
        default="0x3E,0x58,0xEA,0x87,0x33,0x91,0x6D,0x46,0xE7,0x09,0x3E,0xB0,0x93,0xEB,0x09,0x3E,0xB0,0x53,0xEB,0x05")
    parser.add_argument('-a', '--address', help="Address of FE_EMU_CONFIG",
        default="0x4010", type=str)
    parser.add_argument('-w', '--width', help="Elink width", default=8, type=int)
    parser.add_argument('-dw', '--data_width', help="WRDATA width",
        default='32', type=str)
    parser.add_argument('-aw', '--address_width', help="WRADDR width",
        default='14', type=str)
    parser.add_argument('-wew', '--we_width', help="WE width",
        default='8', type=str)

    global args
    args = parser.parse_args()

    reg_addr = int(args.address, 0)
    address_width = int(args.address_width, 0)  
    data_width = int(args.data_width, 0)
    we_width = int(args.we_width, 0)
    pattern = args.pattern
    elink_width = args.width

    patten_generator = PatternGenerator(pattern=pattern,
        elink_width=elink_width, egroup_width=data_width)
    we = 2**we_width - 1


    for addr in tqdm(range(0, 2**address_width)):
        data = ((we << (data_width + address_width + 1)) |
            (addr << (data_width + 1)) |
        patten_generator.next())
        cmd = f'flx-config setraw -b 2 -r 0x{reg_addr:X} -w 64 -v 0x{data:X} > /dev/null'
        # print(cmd)
        os.system(cmd)
        # if addr == 11:
        #     break

    os.system(f'flx-config setraw -b 2 -r 0x{reg_addr:X} -w 64 -v 0 > /dev/null')


if __name__ == '__main__':
    main()
