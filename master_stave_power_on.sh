#!/bin/bash

REGMAP=`flx-config get REG_MAP_VERSION | sed 's/REG_MAP_VERSION=0x//' | cut -c 1-1`

if [ $REGMAP -eq 5 ]
then
    AMAC_ELINK="014"
    PHASE=2
else    
    AMAC_ELINK="03f"
    PHASE=1    
fi

echo "This is phase${PHASE} firmware, AMAC is at elink 0x${AMAC_ELINK}"    

AMAC_RANGE="0 13"

int_handler()
{
    printf "\e[0m\n"
    echo "Ctrl+C"
    printf "\n"
    kill $PPID
    exit 1
}
trap 'int_handler' INT

# Configure and power on every AMAC (lpGBT master only)

BGs=(0x8E8D 0x8F8E 0x8F8E 0x8F8F 0x8F8F 0x8F8E 0x8F8E 0x8E8E 0x8F8D 0x8F8E 0x8F8E 0x8E8D 0x8F8D 0x8D8F 0x8F8F 0x8E8E 0x8F8D 0x8F8E 0x8F8F 0x8F8D 0x8E8D 0x8E8E 0x8E8D 0x8E8D 0x8E8C 0x8F8F 0x8F8D 0x8F8D)

for i in `seq ${AMAC_RANGE}`;
do
    echo Configuring AMAC \#$i
    echo "    ./amac_tool.py -ai $i setid -pi $i -e_out ${AMAC_ELINK}"
    ./amac_tool.py -ai $i setid -pi $i -e_out ${AMAC_ELINK}
    ./amac_tool.py -ai $i setid -pi $i -e_out ${AMAC_ELINK}
    echo "    ./amac_tool.py -ai $i configure -e_out ${AMAC_ELINK} -bg ${BGs[$i]}"
    ./amac_tool.py -ai $i configure -e_out ${AMAC_ELINK} -bg ${BGs[$i]}
done 



