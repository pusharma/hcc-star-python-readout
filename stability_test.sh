#!/bin/bash

# Use example:
# ./stability_test.sh 10 -v -e_dout 05 -e_lcb 10

# The script will launch ./run_all_tests.py and forward all arguments
# (except the first one, which is the number of the iterations you want)

# The output from the failed iterations is saved to stability_test_fails.log

cd "$( dirname "${BASH_SOURCE[0]}" )"
source venv/bin/activate

let "total = $1"
fail_log="stability_test_fails.log"


int_handler()
{
	printf "\e[0m\n"
	echo "Ctrl+C"
	printf "\n"
	echo -e "\e[0mPassed $passes/$total test iterations"
	kill $PPID
	exit 1
}
trap 'int_handler' INT

echo -e "Prints '\e[32m.\e[0m' for an iteration that passed, '\e[31mx\e[0m' for an iteration that failed:"
echo "Will run $total iterations; each iteration can take a while"

let "fails = 0"
let "passes = 0"
echo "" > $fail_log

for (( i=0; i<$total; i++ ))
do
	./run_all_tests.py -v ${@:2} > stability_test.log 2>&1
	#echo $cmd
	#$cmd
	if [ $? -eq 0 ]
	then
		let "passes = $passes + 1"
		printf "\e[32m."
	else
		let "fails = $fails + 1"
		printf "\e[31mx"
		echo "Iteration $i:" >> $fail_log
		cat stability_test.log >> $fail_log
	fi
done

printf "\n"

echo -e "\e[0mPassed $passes/$total test iterations"
