#!/bin/bash

echo "Disable AMAC elinks (link 0)"
# direct mode ToHost, FromHost
# encoding configuration is ignored by AMAC elinks
flx-config set MINI_EGROUP_TOHOST_00_EC_ENCODING=0
flx-config set MINI_EGROUP_FROMHOST_00_EC_ENCODING=0
# turn off the EC elinks
flx-config set MINI_EGROUP_TOHOST_00_EC_ENABLE=0
flx-config set MINI_EGROUP_FROMHOST_00_EC_ENABLE=0
# turn off IC elinks
flx-config set MINI_EGROUP_TOHOST_00_IC_ENABLE=0
flx-config set MINI_EGROUP_FROMHOST_00_IC_ENABLE=0

echo "Disable AMAC elinks (link 1)"
# direct mode ToHost, FromHost
# encoding configuration is ignored by AMAC elinks
flx-config set MINI_EGROUP_TOHOST_01_EC_ENCODING=0
flx-config set MINI_EGROUP_FROMHOST_01_EC_ENCODING=0
# turn off the EC elinks
flx-config set MINI_EGROUP_TOHOST_01_EC_ENABLE=0
flx-config set MINI_EGROUP_FROMHOST_01_EC_ENABLE=0
# turn off IC elinks
flx-config set MINI_EGROUP_TOHOST_01_IC_ENABLE=0
flx-config set MINI_EGROUP_FROMHOST_01_IC_ENABLE=0

# todo: loop over 7 egroups, 2 links
echo "Disable decoding elinks (lpGBT link 0)"

flx-config set DECODING_LINK00_EGROUP0_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK00_EGROUP1_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK00_EGROUP2_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK00_EGROUP3_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK00_EGROUP4_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK00_EGROUP5_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK00_EGROUP6_CTRL_EPATH_ENA=0x00

echo "Disable decoding elinks (lpGBT link 1)"
flx-config set DECODING_LINK01_EGROUP0_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK01_EGROUP1_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK01_EGROUP2_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK01_EGROUP3_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK01_EGROUP4_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK01_EGROUP5_CTRL_EPATH_ENA=0x00
flx-config set DECODING_LINK01_EGROUP6_CTRL_EPATH_ENA=0x00

echo "Disable encoding elinks (lpGBT link 0)"
flx-config set ENCODING_LINK00_EGROUP0_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK00_EGROUP1_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK00_EGROUP2_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK00_EGROUP3_CTRL_EPATH_ENA=0x00

echo "Disable encoding elinks (lpGBT link 1)"
flx-config set ENCODING_LINK01_EGROUP0_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK01_EGROUP1_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK01_EGROUP2_CTRL_EPATH_ENA=0x00
flx-config set ENCODING_LINK01_EGROUP3_CTRL_EPATH_ENA=0x00
