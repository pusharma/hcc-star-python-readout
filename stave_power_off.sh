#!/bin/bash

REGMAP=`flx-config get REG_MAP_VERSION | sed 's/REG_MAP_VERSION=0x//' | cut -c 1-1`

if [ $REGMAP -eq 5 ]
then
    AMAC_ELINK1="014"
    AMAC_ELINK2="054"
else    
    AMAC_ELINK1="03f"
    AMAC_ELINK2="0Bf"
fi

AMAC_RANGE="0 13"

# Turn off every AMAC 

for i in `seq ${AMAC_RANGE}`;
do
    echo "Turning off AMAC \#$i - master"
    echo "    ./amac_tool.py -ai $i write -ri 40 -rd 0 -e_out ${AMAC_ELINK1}"
	./amac_tool.py -ai $i write -ri 40 -rd 0 -e_out ${AMAC_ELINK1}
	echo "    ./amac_tool.py -ai $i write -ri 41 -rd 0 -e_out ${AMAC_ELINK1}"
	./amac_tool.py -ai $i write -ri 41 -rd 0 -e_out ${AMAC_ELINK1}

	echo "Turning off AMAC \#$i - slave"
    echo "    ./amac_tool.py -ai $i write -ri 40 -rd 0 -e_out ${AMAC_ELINK2}"
	./amac_tool.py -ai $i write -ri 40 -rd 0 -e_out ${AMAC_ELINK2}
	echo "    ./amac_tool.py -ai $i write -ri 41 -rd 0 -e_out ${AMAC_ELINK2}"
	./amac_tool.py -ai $i write -ri 41 -rd 0 -e_out ${AMAC_ELINK2}
done 