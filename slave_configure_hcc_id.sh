#!/bin/bash

# Assign HCC_IDs to the stave (lpGBT slave only)

REGMAP=`flx-config get REG_MAP_VERSION | sed 's/REG_MAP_VERSION=0x//' | cut -c 1-1`

if [ $REGMAP -eq 5 ]
then
	# phase2 firmware
	ELINKS=("04e" "04e" "04e" "04e" "049" "049" "049" "049" "053" "053" "053" "053" "053" "041")
else    
	# phase1 firmware
	ELINKS=("8A" "8A" "8A" "8A" "86" "86" "86" "86" "8E" "8E" "8E" "8E" "8E" "80")
fi

HCC_IDS=("0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13")
EFUSES=("0x00420d" "0x004212" "0x00422c" "0x00422b" "0x004214" "0x004203" "0x004232" "0x004233" "0x004234" "0x00422e" "0x00422f" "0x001a20" "0x001a38" "0x001a39")


int_handler()
{
    printf "\e[0m\n"
    echo "Ctrl+C"
    printf "\n"
    kill $PPID
    exit 1
}
trap 'int_handler' INT

echo "Assign HCC_IDs on the slave stave"
for i in `seq 0 13`;
do
	echo "Assign HCC_ID=${HCC_IDS[$i]} to chip with eFuseID=${EFUSES[$i]} (TX elink ${ELINKS[$i]})"
	echo "    ./hcc_tool.py set_hcc_id -hi ${HCC_IDS[$i]} -se ${EFUSES[$i]} -e_lcb ${ELINKS[$i]} -nfh direct"
	./hcc_tool.py set_hcc_id -hi ${HCC_IDS[$i]} -se ${EFUSES[$i]} -e_lcb ${ELINKS[$i]} -nfh direct
done
