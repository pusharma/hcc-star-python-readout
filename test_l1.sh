#!/bin/bash

echo "Enable L1 trigger for Strips link 0"
flx-config setraw -r 0xD1E0 -o 1 -w 1 -v 1

echo "Set tag field for simulated L1 frame"
flx-config setraw -r 0xD000 -o 4 -w 7 -v 0x2A

sleep 3

# If the trigger doesn't come out, check this offset
# it's unofficial and unstable, and changes from build to build
echo "Send simulated L1 frame"
flx-config setraw -r 0xD830 -o 0 -w 64 -v 1
