#!/bin/bash

int_handler()
{
	echo ""
	echo "Ctrl+C"
	echo ""
	kill $PPID
	exit 1
}
trap 'int_handler' INT

echo ""
echo "This is a helper script for testing whether Strips elinks receive the"
echo "data from the software when fupload command is issued. Make sure to"
echo "enable all FromHost strips elinks using phase2_strips_enable_test.sh"
echo ""
echo "Please also load and observe ILAs of the Strips link and set up the triggers"
echo "to ensure the data is arriving to the encoders as expected."
echo ""
echo "This script requires working cmem-dump binary, which can be found in"
echo "https://gitlab.cern.ch/atlas-tdaq-felix/flxcard/-/tree/rm-5.0_cmem_dump"
echo "-----------------------------------------------------------------------"
echo "Press enter to continue, Ctrl+C to abort"

read tmp

echo "0xde 0xad 0xbe 0xef" > fdata.txt

# Iterate over all elinks
for (( i=0; i<22; i++ ))
do	
	ELINK=`printf '%x\n' $i`
	echo "Testing elink $ELINK (hex) $i (dec)"
	CMD="fupload -d 0 -b 1024 -e $ELINK -c -r 1 fdata.txt; cmem-dump -n 1"	
	echo $CMD
	eval $CMD
	echo ""		
	echo "Press enter to continue"
	read tmp
done
